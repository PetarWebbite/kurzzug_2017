// Can also be used with $(document).ready()
jQuery(window).load(function() {
  jQuery('.flexslider').flexslider({
    animation: "slide",
    controlNav: false,
      animationSpeed: 2250
  });

    var width = jQuery(window).width();

    if(width<1199){
        if(!jQuery('.navbar-collapse').hasClass('resize')){
            jQuery('.navbar-collapse').addClass('resize').attr('style','width:'+(parseInt(width)-50)+'px');
        }
    }else{
        jQuery('.navbar-collapse').removeClass('resize').attr('style','');
    }

});

jQuery(window).resize(function($){

    var width = jQuery(window).width();

    if(width<1199){
        if(!jQuery('.navbar-collapse').hasClass('resize')){
            jQuery('.navbar-collapse').addClass('resize').attr('style','width:'+(parseInt(width)-50)+'px');
        }else{
            jQuery('.navbar-collapse').attr('style','width:'+(parseInt(width)-50)+'px');
        }

    }else{
        jQuery('.navbar-collapse').removeClass('resize').attr('style','');
    }

});