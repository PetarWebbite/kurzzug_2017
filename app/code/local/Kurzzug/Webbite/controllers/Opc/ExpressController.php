<?php

require_once 'IWD/Opc/controllers/ExpressController.php';

class Kurzzug_Webbite_Opc_ExpressController extends  IWD_Opc_ExpressController{

	/**
	 * Return from PayPal and dispatch customer to order review page
	 */
	public function returnAction(){
		$new_mage = $this->isNewMagento();
		if($new_mage){ // for newer magentos
			if ($this->getRequest()->getParam('retry_authorization') == 'true'
				&& is_array($this->_getCheckoutSession()->getPaypalTransactionData())
			) {
				$this->_forward('placeOrder');
				return;
			}
			
			try {
				$this->_getCheckoutSession()->unsPaypalTransactionData();
				$this->_checkout = $this->_initCheckout();
				$this->_checkout->returnFromPaypal($this->_initToken());

				if ($this->_checkout->canSkipOrderReviewStep()) {
					$this->_forward('placeOrder');
				} else {
					Mage::getSingleton ( 'core/session' )->unsPplRedirect ( );
					$this->_redirect('paypal/express/placeOrder', array('_secure'=>true));
				}
				return;
			} catch (Mage_Core_Exception $e) {
				Mage::getSingleton('checkout/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('checkout/session')->addError($this->__('Unable to process Express Checkout approval.'));
				Mage::logException($e);
			}
			$this->_redirect('checkout/cart');
		}
		else{ // for older magentos
			try {
				$this->_initCheckout();
				$this->_checkout->returnFromPaypal($this->_initToken());
				Mage::getSingleton ( 'core/session' )->unsPplRedirect ( );
				echo "<script>parent.parent.location.href='" . Mage::getUrl('paypal/express/review', array('_secure'=>true)) . "'</script>";
				return;
			}
			catch (Mage_Core_Exception $e) {
				Mage::getSingleton('checkout/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('checkout/session')->addError($this->__('Unable to process Express Checkout approval.'));
				Mage::logException($e);
			}
			$this->_redirect('checkout/cart', array('_secure'=>true));
		}	
	}
	
	/**
	 * Instantiate quote and checkout
	 * @throws Mage_Core_Exception
	 */
	private function _initCheckout(){
		$quote = $this->_getQuote();
		if (!$quote->hasItems() || $quote->getHasError()) {
			$this->getResponse()->setHeader('HTTP/1.1','403 Forbidden');
			Mage::throwException(Mage::helper('paypal')->__('Unable to initialize Express Checkout.'));
		}
		$this->_checkout = Mage::getSingleton($this->_checkoutType, array(
				'config' => $this->_config,
				'quote'  => $quote,
		));
		return $this->_checkout;
	}

	/**
	 * Search for proper checkout token in request or session or (un)set specified one
	 * Combined getter/setter
	 *
	 * @param string $setToken
	 * @return Mage_Paypal_ExpressController|string
	 */
	protected function _initToken($setToken = null){
		if (null !== $setToken) {
			if (false === $setToken) {
				// security measure for avoid unsetting token twice
				if (!$this->_getSession()->getExpressCheckoutToken()) {
					Mage::throwException($this->__('PayPal Express Checkout Token does not exist.'));
				}
				$this->_getSession()->unsExpressCheckoutToken();
			} else {
				$this->_getSession()->setExpressCheckoutToken($setToken);
			}
			return $this;
		}
		if ($setToken = $this->getRequest()->getParam('token')) {
			if ($setToken !== $this->_getSession()->getExpressCheckoutToken()) {
				Mage::throwException($this->__('Wrong PayPal Express Checkout Token specified.'));
			}
		} else {
			$setToken = $this->_getSession()->getExpressCheckoutToken();
		}
		return $setToken;
	}

	/**
	 * PayPal session instance getter
	 *
	 * @return Mage_PayPal_Model_Session
	 */
	private function _getSession(){
		return Mage::getSingleton('paypal/session');
	}

	/**
	 * Return checkout session object
	 *
	 * @return Mage_Checkout_Model_Session
	 */
	private function _getCheckoutSession(){
		return Mage::getSingleton('checkout/session');
	}

	/**
	 * Return checkout quote object
	 *
	 * @return Mage_Sale_Model_Quote
	 */
	private function _getQuote(){
		if (!$this->_quote) {
			$this->_quote = $this->_getCheckoutSession()->getQuote();
		}
		return $this->_quote;
	}

	/**
	 * Redirect to login page
	 *
	 */
	public function redirectLogin()
	{
		$this->setFlag('', 'no-dispatch', true);
		$this->getResponse()->setRedirect(
			Mage::helper('core/url')->addRequestParam(
				Mage::helper('customer')->getLoginUrl(),
				array('context' => 'checkout')
			)
		);
	}

	public function isNewMagento(){
		$mage  = new Mage();
		if (!is_callable(array($mage, 'getEdition'))){
			$edition = 'Community';
		}else{
			$edition = Mage::getEdition();
		}
		unset($mage);

		$version = Mage::getVersionInfo();
		$m1 = $version['major'];
		$m2 = $version['minor'];
		$v = $m1*1000+$m2*10;

		if($edition == 'Enterprise'){
			if($v >= 1140) // 1.14
				return true;
		}
		else
		{
			if($v >= 1090) // 1.9
				return true;
		}

		return false;
	}
}