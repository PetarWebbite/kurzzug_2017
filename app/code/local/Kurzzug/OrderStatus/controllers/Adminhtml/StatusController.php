<?php
/**
 * Created by PhpStorm.
 * User: Iva
 * Date: 27.1.2015.
 * Time: 19:04
 */

class Kurzzug_OrderStatus_Adminhtml_StatusController extends Mage_Adminhtml_Controller_Action{

    public function indexAction(){
        echo "here";
    }

    public function allAction(){
        $orderIds = $this->getRequest()->getParam('order_ids');

        foreach($orderIds as $orderId){
            $order = Mage::getModel('sales/order')->load($orderId);

                $this->_processOrderStatus($order);

        }

        $message = $this->__('Invoices have been generated.');
        Mage::getSingleton('adminhtml/session')->addSuccess($message);

        return $this->_redirectReferer();
    }

    private function _processOrderStatus($order){
//        $invoice = $order->prepareInvoice();
//
//        $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_PAID);
//        $invoice->setState(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
        $status = "paid";
        $email = "true";

//        if($this->_getPaymentMethod($order)=="banktransfer"){
//            $status = "paid";
//            $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_OPEN);
//            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE);
//            $email = false;
//        }

//        if($order->getData('created_at')<"2015-03-02 00:00:00"){
//            $invoice->setData('created_at','2015-03-02 00:00:00');
//        }


//        $invoice->register();
//        Mage::getModel('core/resource_transaction')
//            ->addObject($invoice)
//            ->addObject($invoice->getOrder())
//            ->save();
//
//        $invoice->sendEmail($email, '');


        $this->_changeOrderStatus($order,$status);
        return true;
    }

    private function _getPaymentMethod($order)
    {
        return $order->getPayment()->getMethodInstance()->getCode();
    }

    private function _changeOrderStatus($order,$statusMessage){
        if($statusMessage=="process"){
            $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
            if(!$order->getBaseTotalDue() && !$order->getPaidOn()){
                $order->setPaidOn(date("Y-m-d H:i:s"));
            }
        }

        if($statusMessage=='paid'){
            $statusMessage='';
            $order->setState('processing','paid','', false);
            $order->setPaidOn(date("Y-m-d H:i:s"));
        }
        $order->setBlabLa(111);
        $order->save();
    }

    public function paidAction(){
        $orderId = $this->getRequest()->getParam('order_id');

        $order = Mage::getModel('sales/order')->load($orderId);

        $this->_processOrderStatus($order);

        $message = $this->__('Order status set to paid.');
        Mage::getSingleton('adminhtml/session')->addSuccess($message);

        return $this->_redirectReferer();

    }

    protected function _isAllowed()
    {
        return true;
    }
}