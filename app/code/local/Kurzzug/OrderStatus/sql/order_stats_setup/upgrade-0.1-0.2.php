<?php
/**
 * Created by PhpStorm.
 * User: Iva
 * Date: 16.7.2015.
 * Time: 21:13
 */

$installer = new Mage_Sales_Model_Resource_Setup('core_setup');
$installer->startSetup();


$entities = array(
    'order',
);
$options = array(
    'type'     => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'visible'  => true,
    'required' => false
);

foreach ($entities as $entity) {
    $installer->addAttribute($entity, 'paid_on', $options);
}


$installer->endSetup();