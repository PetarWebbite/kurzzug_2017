<?php
/**
 * Created by PhpStorm.
 * User: Iva
 * Date: 26.1.2015.
 * Time: 19:55
 */

class Kurzzug_OrderStatus_Model_Observer {

    public function implementStatus($observer){
        $order = $observer->getOrder();


            if ($order->canInvoice()){
                $this->_processOrderStatus($order);
            }

//        if($this->_getPaymentMethod($order)=='banktransfer'){
//            if($order->canInvoice()){
//                $this->_processingOrder($order);
//            }
//        }

        if(!$order->getBaseTotalDue() && !$order->getPaidOn()){

        $order->setPaidOn(date("Y-m-d H:i:s"))
            ->setUpdatedAt(date("Y-m-d H:i:s"))
        ;
            $order->setGiberish("bla");
        $order->save();
    }

        return $this;
    }

    private function _getPaymentMethod($order)
    {
        return $order->getPayment()->getMethodInstance()->getCode();
    }

    private function _processingOrder($order){
        $invoice = $order->prepareInvoice();
        $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_OPEN);
        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::NOT_CAPTURE);
//        Zend_Debug::dump($invoice->getData());exit;
        $invoice->register();
        Mage::getModel('core/resource_transaction')
            ->addObject($invoice)
            ->addObject($invoice->getOrder())
            ->save();

        $invoice->sendEmail(false, '');
        $this->_changeOrderStatus($order,'process');
        return true;
    }

    private function _processOrderStatus($order){
        $invoice = $order->prepareInvoice();
        $invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_PAID);
//        $invoice->setState(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
        $invoice->register();
        Mage::getModel('core/resource_transaction')
            ->addObject($invoice)
            ->addObject($invoice->getOrder())
            ->save();

        $invoice->sendEmail(true, '');
        $this->_changeOrderStatus($order,'paid');
        return true;
    }

    private function _changeOrderStatus($order,$statusMessage){
        if($statusMessage=="process"){
            $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
        }

        if($statusMessage=='paid'){
            $statusMessage='';
            $order->setPaidOn(date("Y-m-d H:i:s"));
            $order->setState('processing','paid','', false);
        }
        $order->setGiberish("bla");
        $order->save();
    }

    public function paidOn($observer){
        $order = $observer->getEvent()->getInvoice()->getOrder();


        $order->setGiberish("bla");

        if(!$order->getBaseTotalDue()){

            $ord = Mage::getModel("sales/order")->load($order->getId());
            $ord->setGiberish("bla");
            $ord->setPaidOn(date("Y-m-d H:i:s"))
            ->setUpdatedAt(date("Y-m-d H:i:s"))
                ->save();
        }
    }

    protected function _isAllowed()
    {
        return true;
    }


    public function addPaidButton($observer){
        $block = $observer->getBlock();

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            $order_id = Mage::app()->getRequest()->getParam("order_id");
            $order = Mage::getModel('sales/order')->load($order_id);

            $status = $order->getStatus();

            if($status!='complete' && $status!='closed' && $status!='paid'){
                $message = Mage::helper('kurzzug_orderstatus')->__('Are you sure you want to do this?');
                $block->addButton('paid_order', array(
                    'label' => Mage::helper('kurzzug_orderstatus')->__('Paid'),
                    'onclick' => "confirmSetLocation('{$message}', '{$block->getUrl('*/status/paid')}')",
                    'class' => 'go'
                ));
            }
        }

    }
}