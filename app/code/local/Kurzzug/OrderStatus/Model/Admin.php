<?php
/**
 * Created by PhpStorm.
 * User: Iva
 * Date: 27.1.2015.
 * Time: 19:01
 */

class Kurzzug_OrderStatus_Model_Admin {

    public function addMassAction($observer){
        $block = $observer->getEvent()->getBlock();
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'sales_order')
        {
            $block->addItem('status', array(
                'label' => 'Invoice All',
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/status/all'),
            ));
        }
    }
}