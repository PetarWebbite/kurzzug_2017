<?php

/**
 * Created by PhpStorm.
 * User: webbite
 * Date: 7/15/2016
 * Time: 9:33 AM
 */
class Kurzzug_Shipall_Adminhtml_ShipController extends Mage_Adminhtml_Controller_Sales_Shipment
{

    public function indexAction(){
        echo 'here';
    }

    /**
     * Initialize shipment items QTY
     */
    protected function _getItemQtys()
    {
        $data = $this->getRequest()->getParam('shipment');
        if (isset($data['items'])) {
            $qtys = $data['items'];
        } else {
            $qtys = array();
        }
        return $qtys;
    }


    public function allAction(){
        $orderIds = $this->getRequest()->getParam('order_ids');

        foreach($orderIds as $orderId){
            $order = Mage::getModel('sales/order')->load($orderId);

            $shipment = Mage::getModel('sales/service_order', $order)
                    ->prepareShipment($this->_getItemQtys($order));

            try{
                if(!$order->hasShipments()){
                    $shipment->getOrder()->setIsInProcess(true);
                    $transactionSave = Mage::getModel('core/resource_transaction')
                        ->addObject($shipment)
                        ->addObject($shipment->getOrder())
                        ->save();
//            $this->_processOrderStatus($order);

                    $message = $this->__('Shipments have been generated.');
                    Mage::getSingleton('adminhtml/session')->addSuccess($message);
                }else{
                    $message = $this->__('Shipment exists for order '.$order->getIncrementId());
                    Mage::getSingleton('adminhtml/session')->addNotice($message);
                }
            }catch(Exception $e){
                $message = $this->__($e->getMessage());
                Mage::getSingleton('adminhtml/session')->addError($message);

            }

        }


        return $this->_redirectReferer();
    }

    protected function _isAllowed()
    {
        return true;
    }
}