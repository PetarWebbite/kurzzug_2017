<?php

/**
 * Created by PhpStorm.
 * User: webbite
 * Date: 7/15/2016
 * Time: 9:59 AM
 */
class Kurzzug_Shipall_Model_Admin
{
    public function addMassAction($observer){
        $block = $observer->getEvent()->getBlock();
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'sales_order')
        {
            $block->addItem('ship', array(
                'label' => 'Ship All',
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/ship/all'),
            ));
        }
    }
}