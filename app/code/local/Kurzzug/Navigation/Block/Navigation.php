<?php
/**
 * Created by PhpStorm.
 * User: webbite
 * Date: 5/31/2016
 * Time: 11:01 AM
 */

class Kurzzug_Navigation_Block_Navigation
    extends Mage_Page_Block_Html_Topmenu
{
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';


        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
            if ($child->hasChildren()) {
                $outermostClassCode .= ' data-toggle="dropdown" ';
            }



//echo $attributes;
            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>';

            $name = $this->escapeHtml($child->getName());
            $arr = explode(' ',$name);


            if(count($arr)>1 && $childLevel == 0){
                $last = array_pop($arr);
                $last1 = str_replace("_","&nbsp;",array_pop($arr));
                $arr[]=$last1."<span class='sep'></span><br />".$last;
            }

            $html .= implode(' ',$arr);
            if ($child->hasChildren()) {
//                $html .= ' <b class="caret"></b>';
            }
            $html .= '</span></a>';

            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . ' dropdown-menu"><li><div class="yamm-content text-capitalize"><div class="row"><ul class="col-md-12">';
                if ($childLevel == 0) {

                    $html .= '<li class="level1 level-top-in-dropdown">';
                    $html .= '<a href="'.$child->getUrl().'"><span>';
                    $html .= str_replace("_","&nbsp;",$this->escapeHtml($child->getName()));
                    $html .= '</span></a>';
                    $html .= '</li>';
                    $html .= '<li class="divider"></li>';
                }
                $html .="</ul>";
                $html .= $this->_getMegaHtml($child, $childrenWrapClass);


                $html .= '</div></div></li></ul>';

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            }
            $html .= '</li>';

            $counter++;

        }

        $static = Mage::getStoreConfig('navigation/general/static_block');

//        Zend_Debug::dump($static);

        $html.= strip_tags(Mage::getModel('cms/block')->load($static)->getContent(),'<li><a>');

        return $html;
    }
}