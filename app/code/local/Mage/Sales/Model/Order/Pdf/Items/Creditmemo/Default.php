<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales Order Creditmemo Pdf default items renderer
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Order_Pdf_Items_Creditmemo_Default extends Mage_Sales_Model_Order_Pdf_Items_Abstract
{
    /**
     * Draw process
     */
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();
        $lines  = array();

        // draw Product name
        $lines[0] = array(array(
            'text' => Mage::helper('core/string')->str_split($item->getName(), 35, true, true),
            'feed' => 180,
            'font_size' => 11
        ));

        // draw SKU
        $lines[0][] = array(
            'text'  => Mage::helper('core/string')->str_split($this->getSku($item), 17),
            'feed'  => 75,
            'align' => 'left',
            'font_size' => 11
        );

        // draw QTY
        $lines[0][] = array(
            'text'  => $item->getQty() * 1,
            'feed'  => 50,
            'align' => 'right',
            'font_size' => 11
        );

        // draw item Prices
        $i = 0;
        $prices = $this->getItemPricesForDisplay();
        $price = array();
        $subtotal = array();
        $feedPrice = 355;
        $feedSubtotal = $feedPrice + 210;
        foreach ($prices as $priceData){
//            if (isset($priceData['label'])) {
//                // draw Price label
//                $lines[$i][] = array(
//                    'text'  => $priceData['label'],
//                    'feed'  => $feedPrice,
//                    'align' => 'right'
//                );
//                // draw Subtotal label
//                $lines[$i][] = array(
//                    'text'  => $priceData['label'],
//                    'feed'  => $feedSubtotal,
//                    'align' => 'right'
//                );
//                $i++;
//            }
            // draw Price
            $price[$i]=$priceData['price'];
            // draw Subtotal
            $subtotal[$i]= $priceData['subtotal'];

            $i++;
        }

        $lines[0][]=array(
            'text'=>$price[0],
            'feed'=>$feedPrice,
//            'font'=>'bold',
            'align'=>'left',
            'font_size' => 11
        );

        $lines[0][]=array(
            'text'=>$subtotal[1],
            'feed'=>$feedSubtotal,
//            'font'=>'bold',
            'align'=>'right',
            'font_size' => 11
        );

        // draw Tax
        $lines[0][] = array(
            'text'  => $order->formatPriceTxt($item->getTaxAmount()),
            'feed'  => 435,
//            'font'  => 'bold',
            'align' => 'left',
            'font_size' => 11
        );


        // custom options
        $options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                $lines[][] = array(
                    'text' => Mage::helper('core/string')->str_split(strip_tags($option['label']), 40, true, true),
                    'font' => 'italic',
                    'feed' => 35
                );

                if ($option['value']) {
                    if (isset($option['print_value'])) {
                        $_printValue = $option['print_value'];
                    } else {
                        $_printValue = strip_tags($option['value']);
                    }
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        $lines[][] = array(
                            'text' => Mage::helper('core/string')->str_split($value, 30, true, true),
                            'feed' => 40
                        );
                    }
                }
            }
        }

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 20
        );
        $page = $pdf->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $this->setPage($page);
    }
}
