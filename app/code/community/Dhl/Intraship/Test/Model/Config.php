<?php
/**
 * Config Model Test
 */
class Dhl_Intraship_Test_Model_Config extends EcomDev_PHPUnit_Test_Case
{
    /**
     * @test
     */
    public function getProductTypesForWeightCalculation()
    {
        $store  = Mage::app()->getStore(0)->load(0);
        $config = Mage::getModel('intraship/config');

        $path = 'intraship/packages/global_settings_weight_product_types';

        $store->resetConfig();
        $this->assertSame(array('simple'), $config->getProductTypesForWeightCalculation());

        $store->setConfig($path, 'simple,configurable');
        $this->assertSame(array('simple', 'configurable'), $config->getProductTypesForWeightCalculation());

        $store->resetConfig();
        $this->assertSame(array('simple'), $config->getProductTypesForWeightCalculation());
    }

    /**
     *
     * @test
     * @loadFixture config
     */
    public function isTestmode()
    {
        $this->assertTrue(Mage::getModel('intraship/config')->isTestmode());

        $store = 'bar';
        Mage::app()->getStore()->setConfig('intraship/general/testmode', false);
        Mage::app()->getStore($store)->setConfig('intraship/general/testmode', true);

        $this->assertFalse(Mage::getModel('intraship/config')->isTestmode());
        $this->assertTrue(Mage::getModel('intraship/config')->isTestmode($store));

        Mage::app()->getConfig()->reinit();
    }

    /**
     * @test
     */
    public function getNotifyShipmentErrorsThreshold()
    {
        $config = Mage::getModel('intraship/config');

        // config.xml default:
        $this->assertEquals(15, $config->getNotifyShipmentErrorsThreshold());

        $notificationsDisabled = -1;
        $store  = Mage::app()->getStore(0)->load(0);
        $path = 'intraship/general/notify_shipment_errors_threshold';
        $store->setConfig($path, $notificationsDisabled);
        $this->assertEquals($notificationsDisabled, $config->getNotifyShipmentErrorsThreshold());
    }

    public function testGetTrackingUrl()
    {
        $link = 'http://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=de&idc=%orderNo%';
        $orderNr = '';
        $this->assertNotEquals('testString', Mage::getModel('intraship/config')->getTrackingUrl($orderNr));

        $orderNr = '12345';
        $excepted = str_replace('%orderNo%', $orderNr, $link);
        $this->assertEquals($excepted, Mage::getModel('intraship/config')->getTrackingUrl($orderNr));
    }

    /**
     * @test
     * @loadFixture config
     */
    public function isTrackingNotification()
    {
        $this->assertTrue(Mage::getModel('intraship/config')->isTrackingNotification());
        $this->assertFalse(Mage::getModel('intraship/config')->isTrackingNotification('foo'));
    }

    /**
     * @test
     * @loadFixture config
     */
    public function getTrackingNotificationMessage()
    {
        $this->assertEquals('Default', Mage::getModel('intraship/config')->getTrackingNotificationMessage());
        $this->assertEquals('Bar', Mage::getModel('intraship/config')->getTrackingNotificationMessage('bar'));
    }

    /**
     * @test
     * @loadFixture config
     */
    public function getFormat()
    {
        $this->assertEquals(
            Mage::getModel('intraship/config')->getFormat(),
            Dhl_Intraship_Model_System_Config_Source_Format::A5
        );
    }

    /**
     * @test
     * @loadFixture config
     */
    public function getOrientation()
    {
        $this->assertEquals(
            Mage::getModel('intraship/config')->getOrientation(),
            Dhl_Intraship_Model_System_Config_Source_Orientation::ORIENTATION_PORTRAIT
        );

        $store  = Mage::app()->getStore(0)->load(0);
        $path = 'intraship/label/paper_orientation';
        $store->setConfig($path, Dhl_Intraship_Model_System_Config_Source_Orientation::ORIENTATION_LANDSCAPE);
        $this->assertEquals(
            Mage::getModel('intraship/config')->getOrientation(),
            Dhl_Intraship_Model_System_Config_Source_Orientation::ORIENTATION_LANDSCAPE
        );
    }


    /**
     * @test
     * @loadFixture config
     */
    public function getProfileByPackageCode()
    {
        // EPN (national) profile
        $profile = Mage::getModel('intraship/config')
            ->getProfileByPackageCode(Dhl_Intraship_Model_Config::PACKAGE_EPN);
        $this->assertEquals(
            Mage::getStoreConfig('intraship/epn/standard'),
            $profile->offsetGet('standard')
        );
        $this->assertEquals(
            Mage::getStoreConfig('intraship/epn/go-green'),
            $profile->offsetGet('go-green')
        );

        // BPI (international) profile
        $profile = Mage::getModel('intraship/config')
            ->getProfileByPackageCode(Dhl_Intraship_Model_Config::PACKAGE_BPI);
        $this->assertEquals(
            Mage::getStoreConfig('intraship/bpi/standard'),
            $profile->offsetGet('standard')
        );
        $this->assertEquals(
            Mage::getStoreConfig('intraship/bpi/go-green'),
            $profile->offsetGet('go-green')
        );

        // EPN (national) profile
        $profile = Mage::getModel('intraship/config')
            ->getProfileByPackageCode(Dhl_Intraship_Model_Config::PACKAGE_EPN, 2);
        $this->assertEquals(
            Mage::getStoreConfig('intraship/epn/standard', 2),
            $profile->offsetGet('standard')
        );
        $this->assertEquals(
            Mage::getStoreConfig('intraship/epn/go-green', 2),
            $profile->offsetGet('go-green')
        );

        // BPI (international) profile
        $profile = Mage::getModel('intraship/config')
            ->getProfileByPackageCode(Dhl_Intraship_Model_Config::PACKAGE_BPI, 2);
        $this->assertEquals(
            Mage::getStoreConfig('intraship/bpi/standard', 2),
            $profile->offsetGet('standard')
        );
        $this->assertEquals(
            Mage::getStoreConfig('intraship/bpi/go-green', 2),
            $profile->offsetGet('go-green')
        );
    }

    /**
     * @test
     * @loadFixture config
     */
    public function getAccountBankData()
    {
        $fooData = Mage::getModel('intraship/config')->getAccountBankData();
        $barData = Mage::getModel('intraship/config')->getAccountBankData('bar');

        $this->assertCount(5, $fooData);
        $this->assertCount(5, $barData);

        $this->assertSame($fooData['accountOwner'], 'Magento Foo');
        $this->assertSame($fooData['bankName'], 'Bank');
        $this->assertSame($fooData['iban'], 'DE11999999999999999999');
        $this->assertSame($fooData['bic'], 'FOOXXXXXXXX');
        $this->assertTrue($fooData->offsetExists('note'));
        $this->assertFalse($fooData->offsetExists('accountNumber'));
        $this->assertFalse($fooData->offsetExists('bankCode'));

        $this->assertSame($barData['accountOwner'], 'Magento Bar');
        $this->assertSame($barData['bankName'], 'Banque');
        $this->assertSame($barData['iban'], 'CH22999999999999999999');
        $this->assertSame($barData['bic'], 'BARXXXXXXXX');
        $this->assertTrue($barData->offsetExists('note'));
        $this->assertFalse($barData->offsetExists('accountNumber'));
        $this->assertFalse($barData->offsetExists('bankCode'));
    }

    /**
     * @test
     * @loadFixture config
     */
    public function getPartnerId()
    {
        // profiles from system configuration (test fixture)
        $profiles = array(
            'EPN' => array(
                'standard' => '101',
                'go-green' => '102',
                'express' => '105',
                'go-green-express' => '106',
            ),
            'BPI' => array(
                'standard' => '103',
                'go-green' => '104',
                'express' => '107',
                'go-green-express' => '108',
            ),
        );

        // sandbox profiles from config defaults (fix values)
        $testProfiles = array(
            'EPN' => array(
                'go-green' => '01',
                'standard' => '03',
                'go-green-express' => '98',
                'express' => '99',
            ),
            'BPI' => array(
                'go-green' => '01',
                'standard' => '03',
                'go-green-express' => '98',
                'express' => '99',
            ),
        );

        $config = Mage::getModel('intraship/config');
        foreach ($testProfiles as $profileCode => $partnerIds) {
            foreach ($partnerIds as $profileName => $partnerId) {
                $this->assertEquals($partnerId, $config->getPartnerId($profileCode, $profileName));
            }
        }

        Mage::app()->getStore()->setConfig('intraship/general/testmode', false);
        foreach ($profiles as $profileCode => $partnerIds) {
            foreach ($partnerIds as $profileName => $partnerId) {
                $this->assertEquals($partnerId, $config->getPartnerId($profileCode, $profileName));
            }
        }
    }
}
