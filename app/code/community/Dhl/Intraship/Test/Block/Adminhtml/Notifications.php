<?php
/**
 * Dhl Intraship
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future.
 *
 * PHP version 5
 *
 * @category  Dhl
 * @package   Dhl_Intraship
 * @author    Christoph Aßmann <christoph.assmann@netresearch.de>
 * @copyright 2016 Netresearch GmbH & Co. KG
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://www.netresearch.de/
 */

/**
 * Dhl_Intraship_Test_Block_Adminhtml_Notifications
 *
 * @category Dhl
 * @package  Dhl_Intraship
 * @author   Christoph Aßmann <christoph.assmann@netresearch.de>
 * @license  http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link     http://www.netresearch.de/
 */
class Dhl_Intraship_Test_Block_Adminhtml_Notifications extends EcomDev_PHPUnit_Test_Case
{
    /**
     * @test
     * @loadFixture ../../../../var/fixtures/orders
     * @loadFixture Notifications
     */
    public function loadStatusCodes()
    {
        $collection = Mage::getModel('intraship/shipment')->getCollection();
        $codes = $collection->getStatusCodes(2);
        $this->assertArrayNotHasKey(Dhl_Intraship_Model_Shipment::STATUS_NEW_QUEUED, $codes);
        $this->assertArrayHasKey(Dhl_Intraship_Model_Shipment::STATUS_NEW_RETRY, $codes);
        $this->assertArrayHasKey(Dhl_Intraship_Model_Shipment::STATUS_CANCEL_RETRY, $codes);
        $this->assertArrayHasKey(Dhl_Intraship_Model_Shipment::STATUS_IN_TRANSMISSION, $codes);

        $collection = Mage::getModel('intraship/shipment')->getCollection();
        $codes = $collection->getStatusCodes(3);

        $this->assertArrayNotHasKey(Dhl_Intraship_Model_Shipment::STATUS_NEW_QUEUED, $codes);
        $this->assertArrayHasKey(Dhl_Intraship_Model_Shipment::STATUS_NEW_RETRY, $codes);
        $this->assertArrayNotHasKey(Dhl_Intraship_Model_Shipment::STATUS_CANCEL_RETRY, $codes);
        $this->assertArrayHasKey(Dhl_Intraship_Model_Shipment::STATUS_IN_TRANSMISSION, $codes);
    }

    /**
     * @test
     * @loadFixture ../../../../var/fixtures/orders
     * @loadFixture Notifications
     */
    public function getShipmentsMessage()
    {
        $urlMock = $this->getModelMock('adminhtml/url', array('getUrl'));
        $urlMock
            ->expects($this->any())
            ->method('getUrl')
            ->willReturn('foo')
        ;
        $this->replaceByMock('model', 'adminhtml/url', $urlMock);


        /** @var DHL_Intraship_Block_Adminhtml_Notifications $block */
        $block = Mage::app()->getLayout()->createBlock('intraship/adminhtml_notifications');

        $messageOne   = $block->getShipmentsMessage(-1); // invalid threshold, no notification
        $messageTwo   = $block->getShipmentsMessage(0); // notify about each and every failure
        $messageThree = $block->getShipmentsMessage(8); // notify on eight or more failures
        $messageFour  = $block->getShipmentsMessage(9); // notify on nine or more failures

        $this->assertEmpty($messageOne);
        $this->assertNotEmpty($messageTwo);
        $this->assertNotEmpty($messageThree);
        $this->assertEmpty($messageFour);
    }
}
