<?php
/**
 * Dhl_Intraship_Model_Mysql4_Shipment_Collection
 *
 * @category  Models
 * @package   Dhl_Intraship
 * @author    Stephan Hoyer <stephan.hoyer@netresearch.de>
 * @copyright Copyright (c) 2010 Netresearch GmbH & Co.KG <http://www.netresearch.de/>
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
class Dhl_Intraship_Model_Mysql4_Shipment_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * @string[]
     */
    protected $_statusRecords;

    protected function _construct()
    {
        $this->_init('intraship/shipment');
    }

    /**
     * Join sales/order to avoid that shipments with deleted orders were processed
     *
     * @return Dhl_Intraship_Model_Mysql4_Shipment_Collection
     */
    public function joinOrderTable()
    {
        $this->getSelect()
            ->join(
                array('so' => $this->getTable('sales/order')),
                'so.entity_id = main_table.order_id',
                '');
        return $this;
    }

    /**
     * Retrieve all status codes, optionally filtered by the minimum number of occurrences.
     *
     * [
     *     $statusCode => $numOccurrences,
     *     …
     * ]
     * @param int $threshold
     * @return string[]
     */
    public function getStatusCodes($threshold = null)
    {
        if (is_null($this->_statusRecords)) {
            $sql = $this->getStatusCodesSql($threshold);
            $this->_statusRecords = $this->getConnection()->fetchPairs($sql);
        }
        return $this->_statusRecords;

    }

    /**
     * Count the number of status codes if they occur at least {$threshold} times.
     *
     * @param int $threshold
     * @return Varien_Db_Select
     */
    public function getStatusCodesSql($threshold = null)
    {
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(Zend_Db_Select::ORDER);
        $countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $countSelect->reset(Zend_Db_Select::COLUMNS);

        $countSelect->columns(array('status', 'count(status) as occurrences'));
        $countSelect->group('status');
        if ($threshold) {
            $countSelect->having('count(status) >= ?', $threshold);
        }

        return $countSelect;
    }
}
