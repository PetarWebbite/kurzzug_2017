<?php
/**
 * Dhl Intraship
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future.
 *
 * PHP version 5
 *
 * @category  Dhl
 * @package   Dhl_Intraship
 * @author    Christoph Aßmann <christoph.assmann@netresearch.de>
 * @copyright 2016 Netresearch GmbH & Co. KG
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://www.netresearch.de/
 */

/**
 * Dhl_Intraship_Model_System_Config_Source_Agecheck
 *
 * @category Dhl
 * @package  Dhl_Intraship
 * @author   Christoph Aßmann <christoph.assmann@netresearch.de>
 * @license  http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link     http://www.netresearch.de/
 */
class Dhl_Intraship_Model_System_Config_Source_Agecheck
{
    /**
     * Options getter
     *
     * @return mixed[]
     */
    public function toOptionArray()
    {
        $optionArray = array();
        $options = $this->toArray();

        foreach ($options as $value => $label) {
            $optionArray[]= array('value' => (string)$value, 'label' => $label);
        }

        return $optionArray;
    }

    /**
     * Get options in "key-value" format
     *
     * @return mixed[]
     */
    public function toArray()
    {
        return array(
            'A18' => Mage::helper('intraship')->__('A18'),
            'A16' => Mage::helper('intraship')->__('A16'),
            '0'   => Mage::helper('adminhtml')->__('No'),
        );
    }
}
