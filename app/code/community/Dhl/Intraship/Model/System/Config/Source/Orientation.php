<?php
/**
 * Dhl_Intraship_Model_System_Config_Source_Orientation
 *
 * @category  Models
 * @package   Dhl_Intraship
 * @autor     Christoph Aßmann <christoph.assmann@netresearch.de>
 * @copyright Copyright (c) 2016 Netresearch GmbH & Co.KG <http://www.netresearch.de/>
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
class Dhl_Intraship_Model_System_Config_Source_Orientation
{
    const ORIENTATION_PORTRAIT = 'P';
    const ORIENTATION_LANDSCAPE = 'L';

    /**
     * Obtain array with label orientation options.
     *
     * @return mixed[]
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::ORIENTATION_PORTRAIT,
                'label' => Mage::helper('intraship/data')->__('Portrait'),
            ),
            array(
                'value' => self::ORIENTATION_LANDSCAPE,
                'label' => Mage::helper('intraship/data')->__('Landscape'),
            ),
        );
    }
}