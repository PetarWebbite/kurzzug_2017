<?php
/**
 * Dhl Intraship
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future.
 *
 * PHP version 5
 *
 * @category  Dhl
 * @package   Dhl_Intraship
 * @author    Christoph Aßmann <christoph.assmann@netresearch.de>
 * @copyright 2016 Netresearch GmbH & Co. KG
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      http://www.netresearch.de/
 */

/**
 * Dhl_Intraship_Model_Return_Receiver
 *
 * @category Dhl
 * @package  Dhl_Intraship
 * @author   Christoph Aßmann <christoph.assmann@netresearch.de>
 * @license  http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link     http://www.netresearch.de/
 */
class Dhl_Intraship_Model_Return_Receiver
{
    const CONFIG_XML_PATH_RECEIVER_USE_SHIPPER = 'intraship/return/receiver_use_shipper';

    const CONFIG_XML_PATH_SHIPPER_FIRSTNAME = 'intraship/shipper/companyName1';
    const CONFIG_XML_PATH_SHIPPER_LASTNAME = 'intraship/shipper/companyName2';
    const CONFIG_XML_PATH_SHIPPER_PHONE = 'intraship/shipper/phone';
    const CONFIG_XML_PATH_SHIPPER_EMAIL = 'intraship/shipper/email';
    const CONFIG_XML_PATH_SHIPPER_CONTACTPERSON = 'intraship/shipper/contactPerson';
    const CONFIG_XML_PATH_SHIPPER_STREETNAME = 'intraship/shipper/streetName';
    const CONFIG_XML_PATH_SHIPPER_STREETNUMBER = 'intraship/shipper/streetNumber';
    const CONFIG_XML_PATH_SHIPPER_ZIP = 'intraship/shipper/zip';
    const CONFIG_XML_PATH_SHIPPER_CITY = 'intraship/shipper/city';
    const CONFIG_XML_PATH_SHIPPER_COUNTRY = 'intraship/shipper/countryISOCode';

    const CONFIG_XML_PATH_RECEIVER_FIRSTNAME = 'intraship/return/receiver_firstname';
    const CONFIG_XML_PATH_RECEIVER_LASTNAME = 'intraship/return/receiver_lastname';
    const CONFIG_XML_PATH_RECEIVER_PHONE = 'intraship/return/receiver_phone';
    const CONFIG_XML_PATH_RECEIVER_EMAIL = 'intraship/return/receiver_email';
    const CONFIG_XML_PATH_RECEIVER_CONTACTPERSON = 'intraship/return/receiver_contact';
    const CONFIG_XML_PATH_RECEIVER_STREETNAME = 'intraship/return/receiver_street';
    const CONFIG_XML_PATH_RECEIVER_STREETNUMBER = 'intraship/return/receiver_streetnumber';
    const CONFIG_XML_PATH_RECEIVER_ZIP = 'intraship/return/receiver_zip';
    const CONFIG_XML_PATH_RECEIVER_CITY = 'intraship/return/receiver_city';
    const CONFIG_XML_PATH_RECEIVER_COUNTRY = 'intraship/return/receiver_country';
    const CONFIG_XML_PATH_RECEIVER_NOTE = 'intraship/return/receiver_note';

    public $companyPersonFirstname;
    public $companyPersonLastname;

    public $communicationPhone;
    public $communicationEmail;
    public $communicationContactPerson;

    public $addressStreetName;
    public $addressStreetNumber;
    public $addressZip;
    public $addressCity;
    public $addressCountryISOCode;
    public $addressCountry;
    public $addressNote;

    /**
     * Init return receiver.
     *
     * @param mixed $args Store context
     */
    public function __construct($args = null)
    {
        // standard instantiation
        $store = $args;

        // model factory instantiation
        if (is_array($args)) {
            $store = isset($args['store']) ? $args['store'] : null;
        }

        $useShipper = Mage::getStoreConfigFlag(self::CONFIG_XML_PATH_RECEIVER_USE_SHIPPER, $store);
        if ($useShipper) {
            $this->loadShipper($store);
        } else {
            $this->loadReceiver($store);
        }
    }

    /**
     * Load return receiver using specific receiver data,
     *
     * @param mixed $store
     */
    protected function loadReceiver($store = null)
    {
        $this->companyPersonFirstname = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_FIRSTNAME, $store);
        $this->companyPersonLastname = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_LASTNAME, $store);

        $this->communicationPhone = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_PHONE, $store);
        $this->communicationEmail = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_EMAIL, $store);
        $this->communicationContactPerson = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_CONTACTPERSON, $store);

        $this->addressStreetName = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_STREETNAME, $store);
        $this->addressStreetNumber = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_STREETNUMBER, $store);
        $this->addressZip = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_ZIP, $store);
        $this->addressCity = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_CITY, $store);
        $this->addressCountryISOCode = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_COUNTRY, $store);
        $this->addressCountry = Mage::getModel('directory/country')->load($this->addressCountryISOCode)->getName();
        $this->addressNote = Mage::getStoreConfig(self::CONFIG_XML_PATH_RECEIVER_NOTE, $store);
    }

    /**
     * Load return receiver using shipper data,
     *
     * @param mixed $store
     */
    protected function loadShipper($store = null)
    {
        $this->companyPersonFirstname = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_FIRSTNAME, $store);
        $this->companyPersonLastname = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_LASTNAME, $store);

        $this->communicationPhone = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_PHONE, $store);
        $this->communicationEmail = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_EMAIL, $store);
        $this->communicationContactPerson = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_CONTACTPERSON, $store);

        $this->addressStreetName = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_STREETNAME, $store);
        $this->addressStreetNumber = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_STREETNUMBER, $store);
        $this->addressZip = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_ZIP, $store);
        $this->addressCity = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_CITY, $store);
        $this->addressCountryISOCode = Mage::getStoreConfig(self::CONFIG_XML_PATH_SHIPPER_COUNTRY, $store);
        $this->addressCountry = Mage::getModel('directory/country')->load($this->addressCountryISOCode)->getName();
        $this->addressNote = null;
    }

    /**
     * Obtain array representation of return receiver.
     *
     * @return mixed[]
     */
    public function toArray()
    {
        $returnReceiver = array(
            'Company' => array(
                'Person' => array(
                    'firstname' => $this->companyPersonFirstname,
                    'lastname' => $this->companyPersonLastname,
                ),
            ),
            'Address' => array(
                'streetName'   => $this->addressStreetName,
                'streetNumber' => $this->addressStreetNumber,
                'city' => $this->addressCity,
                'Origin' => array(
                    'country'        => $this->addressCountry,
                    'countryISOCode' => $this->addressCountryISOCode,
                ),
                'note' => $this->addressNote,
            ),
            'Communication' => array(
                'phone'         => $this->communicationPhone,
                'email'         => $this->communicationEmail,
                'contactPerson' => $this->communicationContactPerson,
            ),
        );

        if ($this->addressCountryISOCode == 'DE') {
            $returnReceiver['Address']['Zip'] = array('germany' => $this->addressZip);
        } else {
            $returnReceiver['Address']['Zip'] = array('other' => $this->addressZip);
        }

        return $returnReceiver;
    }
}