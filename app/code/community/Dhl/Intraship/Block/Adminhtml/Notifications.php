<?php
/**
 * DHL_Intraship_Block_Adminhtml_Notifications
 *
 * @category  Dhl
 * @package   Dhl_Intraship
 * @author    Paul Siedler <paul.siedler@netresearch.de>
 * @copyright Copyright (c) 2013 Netresearch GmbH & Co. KG <http://www.netresearch.de/>
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
class DHL_Intraship_Block_Adminhtml_Notifications extends Mage_Adminhtml_Block_Template
{
    /**
     * Retrieve Intraship SEPA notification.
     *
     * @return string Notification if data are missing, empty string otherwise.
     */
    public function getBankDataMessage()
    {
        $message = '';
        $bankData = Mage::getModel('intraship/config')->getAccountBankData();

        if ((trim($bankData->offsetGet('iban')) == '')
            || (trim($bankData->offsetGet('bic')) == '')
        ) {
            // Display a notification if neither BIC nor IBAN are given
            $message = "Due to SEPA agreement you are required to update your"
                . " bank account data for DHL cash on delivery shipments."
                . " BIC and IBAN are now required fields.";
        }

        return $message;
    }

    /**
     * Retrieve information on shipment queues.
     *
     * @param int $threshold
     * @return string
     */
    public function getShipmentsMessage($threshold = null)
    {
        $message = '';
        if (!is_numeric($threshold) || $threshold < 0) {
            return $message;
        }

        // retrieve status codes with translated status texts
        $shipmentOrder = Mage::getModel('intraship/shipment');
        $failureCodes = array_intersect_key(
            $shipmentOrder->getStatuses(),
            array_flip(array(
                Dhl_Intraship_Model_Shipment::STATUS_NEW_RETRY,
                Dhl_Intraship_Model_Shipment::STATUS_CANCEL_RETRY,
                Dhl_Intraship_Model_Shipment::STATUS_IN_TRANSMISSION,
            ))
        );

        // query status codes with more than $threshold occurrences
        $collection = $shipmentOrder->getCollection();
        $collection->addFieldToFilter('status', array('in' => array_keys($failureCodes)));
        $codes = $collection->getStatusCodes();
        if (array_sum($codes) < $threshold) {
            return $message;
        }

        // build links to shipments grid
        $codes = array_keys($codes);
        $shipmentLinks = array_map(function ($code) use ($failureCodes) {
            $filter = base64_encode(sprintf('intraship_status=%d', $code));
            $url = Mage::helper('adminhtml')->getUrl(
                'adminhtml/sales_shipment/index', array('filter' => $filter)
            );
            return sprintf('<a href="%s">%s</a>', $url, $failureCodes[$code]);
        }, $codes);


        $message = Mage::helper('intraship/data')->__(
            'The following Intraship shipments need your attention: %s. For further information visit the <a href="%s" target="_blank">FAQ</a>.',
            implode(', ', $shipmentLinks),
            'http://de.nr-apps.com/faq/?cat=31&amp;id=227'
        );

        return $message;
    }

    /**
     * Obtain notification messages.
     *
     * @return string[]
     */
    public function getMessages()
    {
        $messages = array();

        $message = $this->getBankDataMessage();
        if ($message) {
            $messages[]= $message;
        }

        $threshold = Mage::getModel('intraship/config')->getNotifyShipmentErrorsThreshold();
        $message = $this->getShipmentsMessage($threshold);
        if ($message) {
            $messages[]= $message;
        }

        return $messages;
    }
}
