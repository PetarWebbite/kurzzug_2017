<?php
/**
 * Created by PhpStorm.
 * User: mario
 * Date: 20.11.15.
 * Time: 16:28
 */
<?php
// Password to be encrypted for a .htpasswd file
$clearTextPassword = 'webbite123';

// Encrypt password
$password = crypt($clearTextPassword, base64_encode($clearTextPassword));

// Print encrypted password
echo $password;
?>