<?php get_header();
global $bpxl_story_options;

if (have_posts()) : the_post();

    $bpxl_cover = rwmb_meta( 'bpxl_post_cover_show', $args = array('type' => 'checkbox'), $post->ID );

    if( $bpxl_story_options['bpxl_single_meta'] == '0' && $bpxl_story_options['bpxl_show_share_buttons'] == '0' ) { } else { ?>
        <div class="post-meta-top clearfix">
            <div class="container">
                <?php if( $bpxl_story_options['bpxl_single_meta'] == '1' ) {
                    if($bpxl_story_options['bpxl_single_post_meta_options']['1'] == '1') { ?>
                        <div class="post-author-single">
                            <?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '38' );  } ?>
                            <span class="post-author"><?php the_author_posts_link(); ?></span>
                        </div>
                    <?php }
                    if($bpxl_story_options['bpxl_single_post_meta_options']['2'] == '1') { ?>
                        <div class="posted-on">
                            <span class="post-date"><time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php _e( 'on', 'bloompixel' ); echo '&nbsp;'; the_time(get_option( 'date_format' )); ?></time></span>
                        </div>
                    <?php }
                }
                if( $bpxl_story_options['bpxl_show_share_buttons'] == '1' ) { ?>
                    <div class="share-icon">
                        <div class="share-button">
                            <i class="fa fa-share-square-o"></i> <span><?php _e( 'Share Story','bloompixel' ); ?></span>
                        </div>
                        <?php get_template_part('template-parts/share-buttons'); ?>
                    </div>
                <?php }
                if( $bpxl_story_options['bpxl_single_meta'] == '1' ) {
                    if($bpxl_story_options['bpxl_single_post_meta_options']['7'] == '1') { ?>
                        <div class="subscribe-icon">
                            <a href="<?php bloginfo('rss2_url'); ?>"><i class="fa fa-rss"></i> <span><?php _e( 'Subscribe','bloompixel' ); ?></span></a>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    <?php }

    if($bpxl_story_options['bpxl_breadcrumbs'] == '1') { ?>
        <div class="breadcrumbs">
            <div class="container">
                <?php bpxl_breadcrumb(); ?>
            </div>
        </div>
    <?php }

    if($bpxl_story_options['bpxl_single_featured'] == '1') {
        if($bpxl_cover == '1') { ?>
            <div class="cover-box">
                <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                <div data-type="background" data-speed="3" class="cover-image" style="background-image: url( <?php echo esc_url( $url ); ?>);">
                    <div class="cover-heading">
                        <div class="cover-text">
                            <?php
                                if($bpxl_story_options['bpxl_single_meta'] == '1') {
                                    if($bpxl_story_options['bpxl_single_post_meta_options']['3'] == '1') { ?>
                                        <div class="post-cats uppercase">
                                            <span>
                                            <?php $category = get_the_category();
                                            if ($category) {
                                              echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", "bloompixel" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
                                            } ?>
                                            </span>
                                        </div><?php
                                    }
                                }
                            ?>
                            <header>
                                <h2 class="title single-title">
                                    <?php the_title(); ?>
                                </h2>
                            </header><!--.header-->
                            <?php if( $bpxl_story_options['bpxl_single_meta'] == '1' ) { ?>
                                <div class="post-meta">
                                    <?php
                                        if($bpxl_story_options['bpxl_single_post_meta_options']['5'] == '1') { ?>
                                            <span class='read-time'><?php echo esc_attr( bpxl_estimated_reading_time() ); ?></span>
                                        <?php }
                                        if($bpxl_story_options['bpxl_single_post_meta_options']['6'] == '1') { ?>
                                            <span class="post-comments"><?php comments_popup_link( __( 'Leave a Comment', 'bloompixel' ), __( '1 Comment', 'bloompixel' ), __( '% Comments', 'bloompixel' ), 'comments-link', __( 'Comments are off', 'bloompixel' )); ?></span>
                                        <?php }
                                        edit_post_link( __( 'Edit', 'bloompixel' ), '<span class="edit-link"><i class="fa fa-pencil-square-o"></i> ', '</span>' ); ?>
                                </div>
                            <?php } ?>
                        </div><!--.cover-text-->
                    </div><!--.cover-heading-->
                </div>
            </div><!--.cover-box-->
        <?php }
    } ?>
    <div class="main-wrapper">
		<div class="main-content <?php bpxl_layout_class(); ?>">
			<div class="content-area single-content-area">
                <div id="content" class="content content-single">
                    <?php
                        $bpxl_single_enabled_elements = $bpxl_story_options['bpxl_single_post_layout']['enabled'];

                        rewind_posts(); while (have_posts()) : the_post();

                        foreach( $bpxl_single_enabled_elements as $bpxl_single_element_key => $bpxl_single_element ) {
                            get_template_part( 'template-parts/'.$bpxl_single_element_key );
                        }

                        // Comments
                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        }

                        endwhile;

                        else :
                            // If no content, include the "No posts found" template.
                            get_template_part( 'template-parts/post-formats/content', 'none' );

                        endif;
                    ?>
                </div>
                <?php
                    $bpxl_single_layout = rwmb_meta( 'bpxl_layout', $args = array('type' => 'image_select'), get_the_ID() );
		
                    if( !empty($bpxl_single_layout) ) {
                        if ( $bpxl_single_layout == 'cslayout' || $bpxl_single_layout == 'sclayout' ) {
                            get_sidebar();
                        } else { }
                    } else if( $bpxl_story_options['bpxl_single_layout'] != 'flayout' ) {
                        get_sidebar();
                    } else { }
                ?>
			</div>
		</div><!--.main-content-->
<?php get_footer();?>