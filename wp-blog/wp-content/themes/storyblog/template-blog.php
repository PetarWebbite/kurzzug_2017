<?php
/*
Template Name: Layout Styles for Demo
*/
?>
<?php /* HEADER START */ ?>
<!DOCTYPE html>
<?php global $bpxl_story_options; ?>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<title itemprop="name"><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if (!empty($bpxl_story_options['bpxl_favicon']['url'])) { ?>
<link rel="icon" href="<?php echo esc_url( $bpxl_story_options['bpxl_favicon']['url'] ); ?>" type="image/x-icon" />
<?php } ?>
<meta name="viewport" content="width=device-width" />
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
	<?php 
		$demo_style = $_GET['layout'];
		$bpxl_header_style = $_GET['header'];

        if ( $bpxl_header_style = '2' ) {
	?>
    <style>
        .main-navigation { margin:25px 0 }
        .logo-wrap { margin:30px 0 }
    </style>
    <?php } ?>
</head>

<?php
if ( ! function_exists( 'demo_layout_class' ) ) :
	function demo_layout_class() {
		global $bpxl_story_options;
		
		$demo_style = $_GET['layout'];
		
		$bpxl_class = '';
        $bpxl_class = $demo_style;
		
		echo $bpxl_class;
	}
endif;
?>

<body id="blog" <?php body_class('main'); ?> itemscope itemtype="http://schema.org/WebPage">
		<div class="main-container">
            <div class="side-nav">
                    <div id="close-button"><i class="fa fa-times"></i></div>
                <?php get_search_form(); ?>
                <div id="sidebar">
                    <?php 
                        if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('Primary Sidebar') );
                    ?>
                </div>
            </div>
            <?php if ( is_single() ) { ?>
            <progress class="reading-progress" value="0"></progress>
            <?php } ?>
			<div class="menu-pusher">
				<!-- START HEADER -->
                <?php 
					if ( !empty($bpxl_header_style) )  {
						$bpxl_header_style = $_GET['header'];
					} else {
						$bpxl_header_style = '1';
					}
					get_template_part('template-parts/header-'.$bpxl_header_style );
				?>
				<!-- END HEADER -->
                
<div class="main-wrapper clearfix">
    <div class="main-content <?php demo_layout_class(); ?>">
        <div class="content-area home-content-area">
            <div id="content" class="content content-home">
                <?php
                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                    elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                    else { $paged = 1; }

                    if($bpxl_story_options['bpxl_home_latest_posts'] == '1') {
                        $recent_cats = $bpxl_story_options['bpxl_home_latest_cat'];
                        $recent_cat = implode(",", $recent_cats);
                        $args = array(
                            'cat' => $recent_cat,
                            'paged' => $paged
                        );
                    } else {
                        if ( $demo_style == 'gridlayout' ) {
                            $args = array(
                                'posts_per_page' => 8,
                                'paged' => $paged
                            );
                        } else {
                            $args = array(
                                'paged' => $paged
                            );
                        }
                    }

                    // The Query
                    query_posts( $args );

                    if (have_posts()) : while (have_posts()) : the_post();

                    /*
                     * Include the post format-specific template for the content. If you want to
                     * use this in a child theme, then include a file called called content-___.php
                     * (where ___ is the post format) and that will be used instead.
                     */

                    get_template_part( 'template-parts/post-formats/content', get_post_format() );

                    endwhile;

                    else:
                        // If no content, include the "No posts found" template.
                        get_template_part( 'template-parts/post-formats/content', 'none' );

                    endif;
                ?>
            </div><!--content-->
            <?php 
                // Previous/next page navigation.
                bpxl_paging_nav();
            ?>
        </div><!--content-area-->
    </div><!--.main-content-->
<?php get_footer(); ?>