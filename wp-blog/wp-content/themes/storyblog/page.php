<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage storyblog
 * @since StoryBlog 1.0
 */

get_header();
global $bpxl_story_options;
$bpxl_cover = rwmb_meta( 'bpxl_post_cover_show', $args = array('type' => 'checkbox'), $post->ID );
if($bpxl_cover == '1') { ?>
	<div class="cover-box">
		<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
		<div data-type="background" data-speed="3" class="cover-image" style="background-image: url( <?php echo esc_url( $url ); ?>);">
		</div>
	</div><!--.cover-box-->
<?php }
if($bpxl_story_options['bpxl_breadcrumbs'] == '1') { ?>
    <div class="breadcrumbs">
        <div class="container">
            <?php bpxl_breadcrumb(); ?>
        </div>
    </div>
<?php } ?>
<div class="main-wrapper">
    <div class="main-content <?php bpxl_layout_class(); ?>">
        <div class="content-area single-content-area">
            <div class="content content-page">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="page-content">
                        <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                            <header>
                                <h1 class="title page-title"><?php the_title(); ?></h1>
                            </header>
                            <?php
                                if($bpxl_story_options['bpxl_single_featured'] == '1') {
                                    if($bpxl_cover == '0' || $bpxl_cover == '') {
                                        if ( has_post_thumbnail() ) { ?>
                                            <div class="featured-single clearfix"><?php the_post_thumbnail('featured'); ?></div>
                                        <?php }
                                    }
                                }
                            ?>
                            <div class="post-content single-page-content">
                                <?php the_content(); ?>
                                <?php edit_post_link( __( 'Edit', 'bloompixel' ), '<span class="edit-link">', '</span>' ); ?>
								
                                <?php wp_link_pages('before=<div class="pagination">&after=</div>'); ?>
                            </div>
                        </article><!--blog post-->
                    </div>	
                    <?php
                        comments_template();
                        
                        endwhile;

                        else :
                            // If no content, include the "No posts found" template.
                            get_template_part( 'template-parts/post-formats/content', 'none' );
                        endif;
                    ?>
            </div><!--.content-page-->
        </div><!--.content-area-->
    </div><!--.main-content-->
<?php get_footer();?>