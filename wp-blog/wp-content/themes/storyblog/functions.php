<?php	
// Load Localization Files



$lang_dir = get_template_directory() . '/lang';
load_theme_textdomain('bloompixel', $lang_dir);

  /*function transparent_theme_setup() {
    load_theme_textdomain( 'bloompixel', get_template_directory() . '/lang' );

    $locale = get_locale();
    $locale_file = get_template_directory() . "/lang/$locale.php";

    if ( is_readable( $locale_file ) ) {
        require_once( $locale_file );
    }
}
add_action( 'after_setup_theme', 'transparent_theme_setup' );*/


 


/*load_theme_textdomain( 'bloompixel', get_template_directory() . '/lang' );

    $locale = get_locale();
    $locale_file = get_template_directory() . "/lang/$locale.po";
    
    if ( is_readable( $locale_file ) ) {
        require_once( $locale_file );
    }*/




if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/inc/options/ReduxCore/framework.php' ) ) {
	require_once( dirname( __FILE__ ) . '/inc/options/ReduxCore/framework.php' );
}
if ( !isset( $bpxl_story_options ) && file_exists( dirname( __FILE__ ) . '/inc/options/settings/settings-config.php' ) ) {
	require_once( dirname( __FILE__ ) . '/inc/options/settings/settings-config.php' );
}

// Custom template functions for this theme.
require get_template_directory() . '/inc/template-functions.php';

/*-----------------------------------------------------------------------------------*/
/* Sets up the content width value based on the theme's design and stylesheet.
/*-----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) ) {
	$content_width = 970;
}

/*-----------------------------------------------------------------------------------*/
/* Sets up theme defaults and registers the various WordPress features that
/* UBlog supports.
/*-----------------------------------------------------------------------------------*/
function bpxl_theme_setup() {
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	
	// This theme supports the following post formats.
	//add_theme_support( 'post-formats', array( 'audio', 'video', 'image', 'gallery', 'link', 'quote', 'status', 'chat', 'aside' ) );
	
	// Register WordPress Custom Menus
	add_theme_support( 'menus' );
	register_nav_menu( 'main-menu', __( 'Main Menu', 'bloompixel' ) );
	
	// Register Post Thumbnails
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 200, 200, true );
	add_image_size( 'featured', 970, 355, true ); //featured
	add_image_size( 'related', 375, 200, true ); //related
	add_image_size( 'widgetthumb', 65, 65, true ); //widgetthumb
    
    /*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
    
    /*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
}
add_action( 'after_setup_theme', 'bpxl_theme_setup' );

/*-----------------------------------------------------------------------------------*/
/*	Post Meta Boxes
/*-----------------------------------------------------------------------------------*/
// Re-define meta box path and URL
define( 'RWMB_URL', trailingslashit( get_template_directory_uri() . '/inc/meta-box' ) );
define( 'RWMB_DIR', trailingslashit( get_template_directory() . '/inc/meta-box' ) );
// Include the meta box script
require_once ( RWMB_DIR . 'meta-box.php' );
// Include the meta box definition (the file where you define meta boxes, see 'demo/demo.php')
include_once(get_template_directory() . '/inc/meta-box/meta-boxes.php');

get_template_part('inc/category-meta/Tax-meta-class/class-usage');

/*-----------------------------------------------------------------------------------*/
/*	Add Stylesheets
/*-----------------------------------------------------------------------------------*/
function bpxl_stylesheets() {
	global $bpxl_story_options;
	wp_enqueue_style( 'storyblog-style', get_stylesheet_uri() );
	
	// Color Scheme
	if (is_category()) {
		$category_ID = get_query_var('cat');
		
		$cat_color_1 = get_tax_meta($category_ID,'bpxl_color_field_id');
	}
	
	// Color Scheme 1
	$color_scheme = "";
	if (is_category()) {
		if (strlen($cat_color_1) > 2 ) {
			$color_scheme = $cat_color_1;
		} else { $color_scheme = $bpxl_story_options['bpxl_color_one']; }
	} elseif ($bpxl_story_options['bpxl_color_one'] != '') {
		$color_scheme = $bpxl_story_options['bpxl_color_one'];
	}
    
    // Navigation Link Colors
    $bpxl_nav_link_color = $bpxl_story_options['bpxl_nav_link_color']['regular'];
	
	// Layout Options
	$bpxl_custom_css = '';
	
	// Hex to RGB
	$bpxl_hex = $color_scheme;
	list($bpxl_r, $bpxl_g, $bpxl_b) = sscanf($bpxl_hex, "#%02x%02x%02x");
	
	// Custom CSS
	if ($bpxl_story_options['bpxl_custom_css'] != '') {
		$bpxl_custom_css = $bpxl_story_options['bpxl_custom_css'];
	}
	
	$custom_css = "
	.header-search:hover .search-button, .search-button, .nav-menu ul li ul li a:hover, .post-type i, .widget_archive a:hover .cat-count, .cat-item a:hover .cat-count, .tagcloud a:hover, .pagination .current, .pagination a:hover, subscribe-widget input[type='submit'], #wp-calendar caption, #wp-calendar td#today, #commentform #submit, .wpcf7-submit, .subscribe-widget input[type='submit'], .jetpack_subscription_widget input[type='submit'], .owl-controls .owl-page.active span, .owl-controls .owl-page:hover span { background-color:{$color_scheme}; }
	a, a:hover, .title a:hover, .main-nav .sub-menu .current-menu-parent > a, .main-nav .sub-menu .current-menu-item > a, .post-cats a:hover, .sidebar a:hover, .breadcrumbs a:hover, .post-meta .fa, .meta a:hover, .post-meta a:hover, .read-more a, .edit-post a, .comment-reply-link, .relatedPosts .widgettitle a:hover, .error-text { color:{$color_scheme}; }
	.post-cats a, .tagcloud a:hover .post blockquote, .pagination .current, .pagination a:hover { border-color:{$color_scheme}; }
    .sf-arrows .sf-with-ul:after { border-top-color:{$bpxl_nav_link_color}; }
	#wp-calendar th { background: rgba({$bpxl_r},{$bpxl_g},{$bpxl_b}, 0.6) } {$bpxl_custom_css}";
	wp_add_inline_style( 'storyblog-style', $custom_css );
	
	// Font-Awesome CSS
	wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'font-awesome' );
	
	if ($bpxl_story_options['bpxl_rtl'] == '1') {
		// Responsive
		wp_register_style( 'rtl', get_template_directory_uri() . '/css/rtl.css' );
		wp_enqueue_style( 'rtl' );
	}
	
	if ($bpxl_story_options['bpxl_responsive_layout'] == '1') {
		// Responsive
		wp_register_style( 'responsive', get_template_directory_uri() . '/css/responsive.css' );
		wp_enqueue_style( 'responsive' );
	}
}
add_action( 'wp_enqueue_scripts', 'bpxl_stylesheets' );

/*-----------------------------------------------------------------------------------*/
/*	Add JavaScripts
/*-----------------------------------------------------------------------------------*/
function bpxl_scripts() {
global $bpxl_story_options;
    wp_enqueue_script( 'jquery' );
	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
	
	// Sticky Menu
	if ($bpxl_story_options['bpxl_sticky_menu'] == '1') {
		wp_register_script( 'stickymenu', get_template_directory_uri() . '/js/stickymenu.js', array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'stickymenu' );
	}
	
	// Required jQuery Scripts
    wp_register_script( 'theme-scripts', get_template_directory_uri() . '/js/theme-scripts.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'theme-scripts' );
}
add_action( 'wp_enqueue_scripts', 'bpxl_scripts' );

/*-----------------------------------------------------------------------------------*/
/*	Add Admin Scripts
/*-----------------------------------------------------------------------------------*/
function bpxl_admin_scripts() {
    wp_register_script( 'admin-scripts', get_template_directory_uri() . '/js/admin-scripts.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'admin-scripts' );
	
    /*wp_register_script( 'select2', get_template_directory_uri() . '/js/select2.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'select2' );*/
	
	/*wp_register_style( 'select2css', get_template_directory_uri() . '/css/select2css.css' );
	wp_enqueue_style( 'select2css' );*/
	
	wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'font-awesome' );
	
	wp_register_style( 'adminstyle', get_template_directory_uri() . '/css/adminstyle.css' );
	wp_enqueue_style( 'adminstyle' );
    
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script('wp-color-picker');
}
add_action( 'admin_enqueue_scripts', 'bpxl_admin_scripts' );

add_action('admin_init','bpxl_select_register');
add_action('admin_print_scripts-widgets.php', 'bpxl_select_enqueue');
add_action('admin_print_scripts-nav-menus.php', 'bpxl_select_enqueue');

function bpxl_select_register() {
    wp_register_script( 'select2', get_template_directory_uri() . '/js/select2.js', array( 'jquery' ), '1.0', true );
    wp_register_style( 'select2css', get_template_directory_uri() . '/css/select2css.css' );
}

function bpxl_select_enqueue() {
    wp_enqueue_script( 'select2' );
    wp_enqueue_style( 'select2css' );
}

/*-----------------------------------------------------------------------------------*/
/*	Load Widgets
/*-----------------------------------------------------------------------------------*/
// Theme Functions
include("inc/widgets/widget-ad160.php"); // 160x600 Ad Widget
include("inc/widgets/widget-ad300.php"); // 300x250 Ad Widget
include("inc/widgets/widget-ad125.php"); // 125x125 Ad Widget
include("inc/widgets/widget-fblikebox.php"); // Facebook Like Box
include("inc/widgets/widget-flickr.php"); // Flickr Widget
include("inc/widgets/widget-popular-posts.php"); // Popular Posts
include("inc/widgets/widget-cat-posts.php"); // Category Posts
include("inc/widgets/widget-random-posts.php"); // Random Posts
include("inc/widgets/widget-recent-posts.php"); // Recent Posts
include("inc/widgets/widget-social.php"); // Social Widget
include("inc/widgets/widget-subscribe.php"); // Subscribe Widget
include("inc/widgets/widget-tabs.php"); // Tabs Widget
include("inc/widgets/widget-tweets.php"); // Tweets Widget
include("inc/widgets/widget-video.php"); // Video Widget
include("inc/nav-walker.php"); // Nav Walker Class
	
/*-----------------------------------------------------------------------------------*/
/*	Exceprt Length
/*-----------------------------------------------------------------------------------*/
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}
add_filter( 'get_the_excerpt', 'do_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Register Theme Widgets
/*-----------------------------------------------------------------------------------*/
function bpxl_widgets_init() {
	global $bpxl_story_options;
	
	register_sidebar(array(
		'name'          => __( 'Primary Sidebar', 'bloompixel' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar of the theme.', 'bloompixel' ),
		'before_widget' => '<div class="widget sidebar-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title uppercase"><span>',
		'after_title'   => '</span></h3>',
	));
	$bpxl_footer_columns = (!empty($bpxl_story_options['bpxl_footer_columns']) ? $bpxl_story_options['bpxl_footer_columns']: '' );
	if ($bpxl_footer_columns == 'footer_4') {
		$sidebars = array(1, 2, 3, 4);
		foreach($sidebars as $number) {
			register_sidebar(array(
				'name'          => __( 'Footer', 'bloompixel' ) . ' ' . $number,
				'id'            => 'footer-' . $number,
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title uppercase"><span>',
				'after_title'   => '</span></h3>'
			));
		}
	} elseif ($bpxl_footer_columns == 'footer_3') {
		$sidebars = array(1, 2, 3);
		foreach($sidebars as $number) {
			register_sidebar(array(
				'name'          => __( 'Footer', 'bloompixel' ) . ' ' . $number,
				'id'            => 'footer-' . $number,
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title uppercase"><span>',
				'after_title'   => '</span></h3>'
			));
		}
	} elseif ($bpxl_footer_columns == 'footer_2') {
		$sidebars = array(1, 2);
		foreach($sidebars as $number) {
			register_sidebar(array(
				'name'          => __( 'Footer', 'bloompixel' ) . ' ' . $number,
				'id'            => 'footer-' . $number,
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title uppercase"><span>',
				'after_title'   => '</span></h3>'
			));
		}
	} else {
		register_sidebar(array(
			'name'          => __( 'Footer', 'bloompixel' ),
			'id'            => 'footer-1',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title uppercase"><span>',
			'after_title'   => '</span></h3>'
		));
	}
}
add_action( 'widgets_init', 'bpxl_widgets_init' );

/*-----------------------------------------------------------------------------------*/
/*	Breadcrumb
/*-----------------------------------------------------------------------------------*/
function bpxl_breadcrumb() {
    global $bpxl_story_options;
    if ($bpxl_story_options['bpxl_breadcrumbs_type'] == 'yoast_breadcrumb') {
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb();
        }
    } else {
        if (!is_home()) {
            echo '<a href="';
            echo esc_url( home_url() );
            echo '"> <i class="fa fa-home"></i>';
            echo _e('Home','bloompixel');
            echo "</a>";
            if (is_category() || is_single()) {
                echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
                the_category(' &bull; ');
                if (is_single()) {
                    echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
                    the_title();
                }
            } elseif (is_page()) {
                echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
                echo the_title();
            }
        }
    }
}

/*-----------------------------------------------------------------------------------*/
/*	Comments Callback
/*-----------------------------------------------------------------------------------*/
function bpxl_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);
?>
	<li <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
    <div class="comment-header clearfix">
        <div class="comment-author vcard">
            <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment->comment_author_email, 70 ); ?>
            <?php printf(__('<cite class="fn">%s</cite>'), get_comment_author_link()) ?>
        </div>
        <?php if ($comment->comment_approved == '0') : ?>
            <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.','bloompixel') ?></em>
            <br />
        <?php endif; ?>

        <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
            <?php
                /* translators: 1: date, 2: time */
                printf( __('%1$s at %2$s','bloompixel'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)','bloompixel'),'  ','' );
            ?>
        </div>
    </div>
	<div class="commentbody">
		<?php comment_text() ?>
		<div class="reply uppercase">
		<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => __('Reply','bloompixel')))) ?>
		</div>
	</div>
	</div>
<?php }


/*-----------------------------------------------------------------------------------*/
/*	Pagination
/*-----------------------------------------------------------------------------------*/
function bpxl_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '') {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages) { $pages = 1; }
     }

     if(1 != $pages) {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}

/*-----------------------------------------------------------------------------------*/
/*	Custom wp_link_pages
/*-----------------------------------------------------------------------------------*/
function bpxl_wp_link_pages( $args = '' ) {
	$defaults = array(
		'before' => '<div class="pagination" id="post-pagination">' . __( '<p class="page-links-title">Pages:</p>' ), 
		'after' => '</div>',
		'text_before' => '',
		'text_after' => '',
		'next_or_number' => 'number', 
		'nextpagelink' => __( 'Next page', 'bloompixel' ),
		'previouspagelink' => __( 'Previous page', 'bloompixel' ),
		'pagelink' => '%',
		'echo' => 1
	);

	$r = wp_parse_args( $args, $defaults );
	$r = apply_filters( 'wp_link_pages_args', $r );
	extract( $r, EXTR_SKIP );

	global $page, $numpages, $multipage, $more, $pagenow;

	$output = '';
	if ( $multipage ) {
		if ( 'number' == $next_or_number ) {
			$output .= $before;
			for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
				$j = str_replace( '%', $i, $pagelink );
				$output .= ' ';
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= _wp_link_page( $i );
				else
					$output .= '<span class="current-post-page">';

				$output .= $text_before . $j . $text_after;
				if ( $i != $page || ( ( ! $more ) && ( $page == 1 ) ) )
					$output .= '</a>';
				else
					$output .= '</span>';
			}
			$output .= $after;
		} else {
			if ( $more ) {
				$output .= $before;
				$i = $page - 1;
				if ( $i && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $previouspagelink . $text_after . '</a>';
				}
				$i = $page + 1;
				if ( $i <= $numpages && $more ) {
					$output .= _wp_link_page( $i );
					$output .= $text_before . $nextpagelink . $text_after . '</a>';
				}
				$output .= $after;
			}
		}
	}

	if ( $echo )
		echo $output;

	return $output;
}

/*-----------------------------------------------------------------------------------*/
/*	Automatic Theme Updates
/*-----------------------------------------------------------------------------------*/
global $bpxl_story_options;
$username = (!empty($bpxl_story_options['bpxl_envato_user_name']) ? $bpxl_story_options['bpxl_envato_user_name']: '' );
$apikey = (!empty($bpxl_story_options['bpxl_envato_api_key']) ? $bpxl_story_options['bpxl_envato_api_key']: '' );
$author = 'Simrandeep Singh';

load_template( trailingslashit( get_template_directory() ) . 'inc/wp-theme-upgrader/envato-wp-theme-updater.php' );
Envato_WP_Theme_Updater::init( $username, $apikey, $author );
?>