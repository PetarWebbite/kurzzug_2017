<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage storyblog
 * @since StoryBlog 1.0
 */

get_header();

global $bpxl_story_options; ?>
<div class="main-wrapper">
	<div class="cat-cover-box archive-cover-box">
        <h1 class="category-title">
            <?php printf( __( 'Category Archives: <span>%s</span>', 'bloompixel' ), single_cat_title( '', false ) ); ?>
        </h1>
	</div>
    <div class="main-content <?php bpxl_layout_class(); ?>">
        <div class="archive-page">
            <div id="content" class="content-area archive-content-area" role="main">
                <div class="content content-archive">
                    <?php
                        // Start the Loop.
                        if (have_posts()) : while (have_posts()) : the_post();

                            /*
                             * Include the post format-specific template for the content. If you want to
                             * use this in a child theme, then include a file called called content-___.php
                             * (where ___ is the post format) and that will be used instead.
                             */
                            get_template_part( 'template-parts/post-formats/content', get_post_format() );

                        endwhile;

                        else:
                            // If no content, include the "No posts found" template.
                            get_template_part( 'template-parts/post-formats/content', 'none' );

                        endif;
                    ?>
                </div><!--.content-->
                <?php 
                    // Previous/next page navigation.
                    bpxl_paging_nav();
                ?>
            </div><!--#content-->
        </div><!--.archive-page-->
    </div><!--.main-content-->
<?php get_footer(); ?>