<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage storyblog
 * @since StoryBlog 1.0
 */

global $bpxl_story_options;

get_header(); ?>

<div class="main-wrapper">
    <div class="main-content <?php bpxl_layout_class(); ?>">
        <div class="content-area home-content-area">
            <div class="content-home">
                <div class="cat-cover-box archive-cover-box">
                    <h1 class="category-title">
                        <?php printf( __( 'Search Results for: <span>%s</span>', 'bloompixel' ), get_search_query() ); ?>
                    </h1>
                </div>
                <div class="content">
                    <?php
                        // Start the Loop.
                        if (have_posts()) : while (have_posts()) : the_post();

                        /*
                         * Include the post format-specific template for the content. If you want to
                         * use this in a child theme, then include a file called called content-___.php
                         * (where ___ is the post format) and that will be used instead.
                         */
                        get_template_part( 'template-parts/post-formats/content', get_post_format() );

                        endwhile;

                        else:
                            // If no content, include the "No posts found" template.
                            get_template_part( 'template-parts/post-formats/content', 'none' );

                        endif;
                    ?>
                </div><!--content-->
                <?php 
                    // Previous/next page navigation.
                    bpxl_paging_nav();
                ?>
            </div><!--content-page-->
        </div><!--content-area-->
    </div><!--.main-->
<?php get_footer(); ?>