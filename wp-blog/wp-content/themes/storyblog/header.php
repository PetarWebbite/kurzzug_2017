<!DOCTYPE html>
<?php global $bpxl_story_options; $lang_dir = get_template_directory() . '/lang';
load_theme_textdomain('bloompixel', $lang_dir); ?>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<link rel="icon" href="http://shop.kurzzug.de/media/favicon/default/kurzzug_favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="http://shop.kurzzug.de/media/favicon/default/kurzzug_favicon.png" type="image/x-icon" />
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script type="text/javascript">
	jQuery(document).ready(function()
	{
		var span=jQuery(".uppercase>span");
		
		var i=0;
		
		while(i<span.length)
		{
			if (jQuery(span[i]).text()=="Archive")
			{
				jQuery(span[i]).text("Archiv");
				break;
			}
			
			i++;
		}
		
		jQuery(".nav-menu-btn").remove();
		
		jQuery("input:text").removeAttr("kl_virtual_keyboard_secure_input");
		
		if (jQuery(".search-form").length>1)
			jQuery(jQuery(".search-form")[1]).remove();
		
		/*if (window.location.href.indexOf("?s=")!=-1)
		{
			jQuery(".s").css("width","50%");
		}*/
		
		var meta=jQuery(".widget_meta").find("li");
		
		if (meta.length>0)
		{
			jQuery(meta[meta.length-1]).remove();
		}
		
			
	});
	</script>
</head>
<body id="blog" <?php body_class('main'); ?> itemscope itemtype="http://schema.org/WebPage">
		<div class="main-container">
		
            <?php
                if ( is_single() ) { ?>
                    <progress class="reading-progress" value="0"></progress>
                    <?php
                    $bpxl_single_layout = rwmb_meta( 'bpxl_layout', $args = array('type' => 'image_select'), get_the_ID() );
		
                    if( !empty($bpxl_single_layout) ) {
                        if ( $bpxl_single_layout == 'cslayout' || $bpxl_single_layout == 'sclayout' ) {
                            
                        } else {
                            get_template_part('template-parts/side-nav' );
                        }
                    } else if( $bpxl_story_options['bpxl_single_layout'] != 'flayout' ) {
                        get_template_part('template-parts/side-nav' );
                    } else {
                        get_template_part('template-parts/side-nav' );
                    }
                } else {
                    get_template_part('template-parts/side-nav' );
                }
            ?>
			<div class="menu-pusher">
			
				<!-- START HEADER -->
                <?php 
					if ( !empty($bpxl_story_options['bpxl_header_style']) )  {
						$bpxl_header_style = $bpxl_story_options['bpxl_header_style'];
					} else {
						$bpxl_header_style = '1';
					}
					get_template_part('template-parts/header-'.$bpxl_header_style );
				?>
				<!-- END HEADER -->