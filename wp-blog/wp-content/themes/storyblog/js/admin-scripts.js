jQuery(document).ready(function() {

	// Media Uploader for Author Page
	jQuery('.custom_media_upload').click(function(e) {
	    e.preventDefault();

	    var custom_uploader = wp.media({
	        title: 'Upload Background Image',
	        button: {
	            text: 'Select Background Image'
	        },
	        multiple: false  // Set this to true to allow multiple files to be selected
	    })
	    .on('select', function() {
	        var attachment = custom_uploader.state().get('selection').first().toJSON();
	        jQuery('.author_bg_image').attr('src', attachment.url).show( 400 );
	        jQuery('.author-media-url').val(attachment.url);
	        //jQuery('.custom_media_id').val(attachment.id);
	    })
	    .open();
	});
	jQuery('.author-image-remove').click(function(e) {
		jQuery('.author_bg_image').hide( 400 );
		jQuery('.author-media-url').val('');
	});
});

/*
* Font Ions for Widget Titles
*
*/
jQuery( window ).load(function() {
    function format(state) {
        if (!state.id) return state.text; // optgroup
        return "<i class='fa fa-" + state.id.toLowerCase() + "'></i>" + state.text;
    }
    function initColorPicker( widget ) {
        widget.find( '.title-icon' ).select2( {
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function(m) { return m; }
        });
    }

    function onFormUpdate( event, widget ) {
        initColorPicker( widget );
    }

    jQuery( document ).on( 'widget-added widget-updated', onFormUpdate );

    jQuery( document ).ready( function() {
        jQuery( '#widgets-right .widget:has(.title-icon)' ).each( function () {
            initColorPicker( jQuery( this ) );
        } );
    } );
});


/*
* Color Picker Tool for Widgets
*
*/
jQuery( window ).load(function() {
    function initColorPicker( widget ) {
        widget.find( '.color-picker' ).wpColorPicker( {
            change: _.throttle( function() { // For Customizer
                $(this).trigger( 'change' );
            }, 3000 )
        });
    }

    function onFormUpdate( event, widget ) {
        initColorPicker( widget );
    }

    jQuery( document ).on( 'widget-added widget-updated', onFormUpdate );

    jQuery( document ).ready( function() {
        jQuery( '#widgets-right .widget:has(.color-picker)' ).each( function () {
            initColorPicker( jQuery( this ) );
        } );
    } );
});