<?php
require_once("Tax-meta-class.php");
if (is_admin()){

  $prefix = 'bpxl_';

  $config = array(
    'id' => 'bpxl_cat_meta_box',          // meta box id, unique per meta box
    'title' => 'BloomPixel Meta Box',    // meta box title
    'pages' => array('category'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => false          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
  
  
  /*
   * Initiate your meta box
   */
  $my_meta =  new Tax_Meta_Class($config);
  
  /*
   * Add fields to your meta box
   */
  
  // Category Primary Color
  $my_meta->addColor($prefix.'color_field_id',array('name'=> __('Category Primary Color ','tax-meta')));
  
  $my_meta->Finish();
}
