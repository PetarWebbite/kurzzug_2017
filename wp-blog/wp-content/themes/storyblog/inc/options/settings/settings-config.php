<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux_Framework_sample_config' ) ) {

        class Redux_Framework_sample_config {

            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;

            public function __construct() {

                if ( ! class_exists( 'ReduxFramework' ) ) {
                    return;
                }

                // This is needed. Bah WordPress bugs.  ;)
                if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                    $this->initSettings();
                } else {
                    add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                }

            }

            public function initSettings() {

                // Just for demo purposes. Not needed per say.
                $this->theme = wp_get_theme();

                // Set the default arguments
                $this->setArguments();

                // Set a few help tabs so you can see how it's done
                $this->setHelpTabs();

                // Create the sections and fields
                $this->setSections();

                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }

                // If Redux is running as a plugin, this will remove the demo notice and links
                //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

                // Function to test the compiler hook and demo CSS output.
                // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
                //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);

                // Change the arguments after they've been declared, but before the panel is created
                //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

                // Change the default value of a field after it's been set, but before it's been useds
                //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

                // Dynamically add a section. Can be also used to modify sections/fields
                //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }

            /**
             * This is a test function that will let you see when the compiler hook occurs.
             * It only runs if a field    set with compiler=>true is changed.
             * */
            function compiler_action( $options, $css, $changed_values ) {
                echo '<h1>The compiler hook has run!</h1>';
                echo "<pre>";
                print_r( $changed_values ); // Values that have changed since the last save
                echo "</pre>";
                //print_r($options); //Option values
                //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

                /*
              // Demo of how to use the dynamic CSS and write your own static CSS file
              $filename = dirname(__FILE__) . '/style' . '.css';
              global $wp_filesystem;
              if( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
              WP_Filesystem();
              }

              if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
              }
             */
            }

            /**
             * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
             * Simply include this function in the child themes functions.php file.
             * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
             * so you must use get_template_directory_uri() if you want to use any of the built in icons
             * */
            function dynamic_section( $sections ) {
                //$sections = array();
                $sections[] = array(
                    'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                    'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                    'icon'   => 'el el-icon-paper-clip',
                    // Leave this as a blank section, no options just some intro text set above.
                    'fields' => array()
                );

                return $sections;
            }

            /**
             * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
             * */
            function change_arguments( $args ) {
                //$args['dev_mode'] = true;

                return $args;
            }

            /**
             * Filter hook for filtering the default value of any given field. Very useful in development mode.
             * */
            function change_defaults( $defaults ) {
                $defaults['str_replace'] = 'Testing filter hook!';

                return $defaults;
            }

            // Remove the demo link and the notice of integrated demo from the redux-framework plugin
            function remove_demo() {

                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );

                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }

            public function setSections() {

                /**
                 * Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
                 * */
                // Background Patterns Reader
                $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
                $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
                $sample_patterns      = array();

                if ( is_dir( $sample_patterns_path ) ) :

                    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) :
                        $sample_patterns = array();

                        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                                $name              = explode( '.', $sample_patterns_file );
                                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                                $sample_patterns[] = array(
                                    'alt' => $name,
                                    'img' => $sample_patterns_url . $sample_patterns_file
                                );
                            }
                        }
                    endif;
                endif;

                ob_start();

                $ct          = wp_get_theme();
                $this->theme = $ct;
                $item_name   = $this->theme->get( 'Name' );
                $tags        = $this->theme->Tags;
                $screenshot  = $this->theme->get_screenshot();
                $class       = $screenshot ? 'has-screenshot' : '';

                $customize_title = sprintf( __( 'Customize &#8220;%s&#8221;', 'redux-framework-demo' ), $this->theme->display( 'Name' ) );

                ?>
                <div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
                    <?php if ( $screenshot ) : ?>
                        <?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
                            <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize"
                               title="<?php echo esc_attr( $customize_title ); ?>">
                                <img src="<?php echo esc_url( $screenshot ); ?>"
                                     alt="<?php esc_attr_e( 'Current theme preview', 'redux-framework-demo' ); ?>"/>
                            </a>
                        <?php endif; ?>
                        <img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>"
                             alt="<?php esc_attr_e( 'Current theme preview', 'redux-framework-demo' ); ?>"/>
                    <?php endif; ?>

                    <h4><?php echo $this->theme->display( 'Name' ); ?></h4>

                    <div>
                        <ul class="theme-info">
                            <li><?php printf( __( 'By %s', 'redux-framework-demo' ), $this->theme->display( 'Author' ) ); ?></li>
                            <li><?php printf( __( 'Version %s', 'redux-framework-demo' ), $this->theme->display( 'Version' ) ); ?></li>
                            <li><?php echo '<strong>' . __( 'Tags', 'redux-framework-demo' ) . ':</strong> '; ?><?php printf( $this->theme->display( 'Tags' ) ); ?></li>
                        </ul>
                        <p class="theme-description"><?php echo $this->theme->display( 'Description' ); ?></p>
                        <?php
                            if ( $this->theme->parent() ) {
                                printf( ' <p class="howto">' . __( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'redux-framework-demo' ) . '</p>', __( 'http://codex.wordpress.org/Child_Themes', 'redux-framework-demo' ), $this->theme->parent()->display( 'Name' ) );
                            }
                        ?>

                    </div>
                </div>

                <?php
                $item_info = ob_get_contents();

                ob_end_clean();

                $sampleHTML = '';
                if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
                    Redux_Functions::initWpFilesystem();

                    global $wp_filesystem;

                    $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
                }

                // ACTUAL DECLARATION OF SECTIONS
                $this->sections[] = array(
                'icon' => 'el-icon-cogs',
                'icon_class' => 'icon-large',
                'title' => __('General Settings', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'=>'bpxl_logo',
                        'type' => 'media', 
                        'url'=> true,
                        'title' => __('Custom Logo', 'bloompixel'),
                        'subtitle' => __('Upload a custom logo for your site.', 'bloompixel'),
                        ),  
                    array(
                        'id'=>'bpxl_retina_logo',
                        'type' => 'media', 
                        'url'=> true,
                        'title' => __('Retina Logo', 'bloompixel'),
                        'subtitle' => __('Upload a retina logo for your site.', 'bloompixel'),
                        ),
                    array(
                        'id'=>'bpxl_pagination_type',
                        'type' => 'button_set',
                        'title' => __('Pagination Type', 'bloompixel'), 
                        'subtitle' => __('Select the type of pagination for your site. Choose between Wide and Boxed.', 'bloompixel'),
                        'options' => array('1' => 'Numbered','2' => 'Next/Prev'),//Must provide key => value pairs for radio options
                        'default' => '1'
                        ),  
                    array(
                        'id'=>'bpxl_scroll_btn',
                        'type' => 'switch', 
                        'title' => __('Scroll to Top Button', 'bloompixel'),
                        'subtitle'=> __('Choose this option to show or hide scroll to top button.', 'bloompixel'),
                        "default"       => 1,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),  
                    array(
                        'id'=>'bpxl_rtl',
                        'type' => 'switch',
                        'title' => __('RTL', 'bloompixel'), 
                        'subtitle' => __('Choose the option if your blog\'s language is rtl.', 'bloompixel'),
                        "default"       => 0,
                        'on' => 'Yes',
                        'off' => 'No',
                        ),
                )
            );
            
            $this->sections[] = array(
                'icon' => 'el-icon-website',
                'icon_class' => 'icon-large',
                'title' => __('Layout Settings', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'=>'bpxl_layout',
                        'type' => 'image_select',
                        'compiler'=>true,
                        'title' => __('Homepage Layout', 'bloompixel'), 
                        'subtitle' => __('Select main content and sidebar alignment. <br><br><strong>Note:</strong> These layouts are applied to homepage and search results pages.', 'bloompixel'),
                        'options' => array(
                                'gridlayout' => array('grid' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/grid.png'),
                                'ttlayout' => array('3-3' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/3-3.png'),
                                'altlayout' => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/alt.png'),
                                'fullrow' => array('fullrow' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/fullrow.png'),
                                'rclayout' => array('1-2-1-2' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/1-2-1-2.png'),
                                'otlayout' => array('1-3-1-3' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/1-3-1.png'),
                                'mixlayout' => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/alt.png'),
                            ),
                        'default' => 'gridlayout'
                        ),
                    array(
                        'id'=>'bpxl_archive_layout',
                        'type' => 'image_select',
                        'compiler'=>true,
                        'title' => __('Archives Layout', 'bloompixel'), 
                        'subtitle' => __('Select layout style for archives. <br><br><strong>Note:</strong> These layouts are applied to archives (Category, tags etc).', 'bloompixel'),
                        'options' => array(
                                'gridlayout' => array('grid' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/grid.png'),
                                'ttlayout' => array('3-3' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/3-3.png'),
                                'altlayout' => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/alt.png'),
                                'fullrow' => array('fullrow' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/fullrow.png'),
                                'rclayout' => array('1-2-1-2' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/1-2-1-2.png'),
                                'otlayout' => array('1-3-1-3' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/1-3-1.png'),
                                'mixlayout' => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/alt.png'),
                            ),
                        'default' => 'gridlayout'
                        ),
                    array(
                        'id'=>'bpxl_single_layout',
                        'type' => 'image_select',
                        'compiler'=>true,
                        'title' => __('Single Layout', 'bloompixel'), 
                        'subtitle' => __('Select layout style for single posts pages.', 'bloompixel'),
                        'options' => array(
                                'flayout' => array('alt' => 'Full Width', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/f.png'),
                                'sclayout' => array('alt' => 'Sidebar - Content', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/bc.png'),
                                'cslayout' => array('alt' => 'Content - Sidebar', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/cb.png'),
                            ),
                        'default' => 'flayout'
                        ),
                )
            );
            
            $this->sections[] = array(
                'icon' => 'el-icon-brush',
                'title' => __('Styling Options', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'=>'bpxl_responsive_layout',
                        'type' => 'switch', 
                        'title' => __('Enable Responsive Layout?', 'bloompixel'),
                        'subtitle'=> __('This theme can adopt to different screen resolutions automatically when rsponsive layout is enabled. You can enable or disable the responsive layout.', 'bloompixel'),
                        "default"       => 1,
                        'on' => 'Enabled',
                        'off' => 'Disabled',
                        ),
                    array(
                        'id'=>'bpxl_color_one',
                        'type' => 'color',
                        'title' => __('Primary Color', 'bloompixel'), 
                        'subtitle' => __('Pick primary color scheme for the theme.', 'bloompixel'),
                        'default' => '#ffbb33',
                        'validate' => 'color',
                        'transparent' => false,
                        'ajax_save'=>true
                        ),
                    array(
                        'id'=>'bpxl_body_break',
                        'type' => 'info',
                        'desc' => __('Body', 'bloompixel')
                        ),
                    array(
                        'id'=>'bpxl_body_text_color',
                        'type' => 'color',
                        'title' => __('Body Text Color', 'bloompixel'), 
                        'subtitle' => __('Pick a color for body text.', 'bloompixel'),
                        'output'   => array('body'),
                        'default' => '#000000',
                        'validate' => 'color',
                        'transparent' => false,
                        ),
                    array(
                        'id'=>'bpxl_nav_break',
                        'type' => 'info',
                        'desc' => __('Navigation Menu', 'bloompixel')
                        ),
                    array( 
                        'id'=>'bpxl_nav_bg_color',
                        'type'     => 'background',
                        'output' => array('.main-navigation'),
                        'title' => __('Main Navigation Background', 'bloompixel'), 
                        'subtitle' => __('Pick a background color for main navigation menu of the theme.', 'bloompixel'),
                        'preview_media' => false,
                        'preview' => false,
                        'transparent' => false,
                        'background-image' => false,
                        'background-repeat' => false,
                        'background-size' => false,
                        'background-attachment' => false,
                        'background-position' => false,
                        'default' => array(
                                'background-color'  => '#ffffff',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_nav_link_color',
                        'type' => 'link_color',
                        'output' => array('.main-nav .current-menu-parent > a, .main-nav .current-page-parent > a, .main-nav .current-menu-item > a, .main-nav a'),
                        'title' => __('Main Navigation Link Color', 'bloompixel'), 
                        'subtitle' => __('Pick a link color for the main navigation.', 'bloompixel'),
                        'default'  => array(
                            'regular'  => '#9b9b9b',
                            'hover'    => '#000000',
                        ),
                        'validate' => 'color',
                        'transparent' => false,
                        'active' => false,
                        ),
                    array(
                        'id'=>'bpxl_nav_button_color',
                        'type' => 'color',
                        'output' => array('.menu-btn'),
                        'title' => __('Mobile Menu Button Color', 'bloompixel'), 
                        'subtitle' => __('Pick a color for buttons that appears on mobile navigation.', 'bloompixel'),
                        'default' => '#000000',
                        'validate' => 'color',
                        'transparent' => false,
                        ),
                    array(
                        'id'=>'bpxl_nav_break',
                        'type' => 'info',
                        'desc' => __('Header', 'bloompixel')
                        ),
                    array( 
                        'id'=>'bpxl_header_bg_color',
                        'type'     => 'background',
                        'output' => array('.main-header'),
                        'title' => __('Header Background', 'bloompixel'), 
                        'subtitle' => __('Pick a background color for header of the theme.', 'bloompixel'),
                        'preview_media' => true,
                        'preview' => false,
                        'transparent' => false,
                        'default' => array(
                                'background-color'  => '#ffffff',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_logo_color',
                        'type' => 'color',
                        'output' => array('.header #logo a'),
                        'title' => __('Logo Color', 'bloompixel'), 
                        'subtitle' => __('Pick a color for logo.', 'bloompixel'),
                        'default' => '#000000',
                        'validate' => 'color',
                        'transparent' => false,
                        ),
                    array(
                        'id'             => 'bpxl_logo_margin',
                        'type'           => 'spacing',
                        'output'         => array('.logo-wrap'),
                        'mode'           => 'margin',
                        'units'          => array('px'),
                        'units_extended' => 'false',
                        'left' => 'false',
                        'right' => 'false',
                        'title'          => __('Logo Margin', 'bloompixel'),
                        'subtitle'       => __('Change distance from top and bottom of logo.', 'bloompixel'),
                        'default'            => array(
                            'margin-top'     => '40px',
                            'margin-bottom'  => '40px',
                            'units'          => 'px',
                            )
                        ),
                    array(
                        'id'             => 'bpxl_nav_menu_margin',
                        'type'           => 'spacing',
                        'output'         => array('.main-navigation'),
                        'mode'           => 'margin',
                        'units'          => array('px'),
                        'units_extended' => 'false',
                        'left' => 'false',
                        'right' => 'false',
                        'title'          => __('Navigation Menu Margin', 'bloompixel'),
                        'subtitle'       => __('Change distance from top and bottom of navigation menu.', 'bloompixel'),
                        'default'            => array(
                            'margin-top'     => '0px',
                            'margin-bottom'  => '0px',
                            'units'          => 'px',
                            )
                        ),
                    array(
                        'id'=>'bpxl_post_box_break',
                        'type' => 'info',
                        'desc' => __('Content', 'bloompixel')
                        ),
                    array(
                        'id'       => 'bpxl_post_title_color',
                        'type'     => 'color',
                        'title'    => __('Single Post Title Color', 'bloompixel'), 
                        'subtitle' => __('Pick a color for the title of single posts.', 'bloompixel'),
                        'output'   => array('.single-title'),
                        'default'  => '#000000',
                        'validate' => 'color',
                        'transparent' => false,
                        ),
                    array(
                        'id'       => 'bpxl_page_title_color',
                        'type'     => 'color',
                        'title'    => __('Page Title Color', 'bloompixel'), 
                        'subtitle' => __('Pick a color for title of page.', 'bloompixel'),
                        'output'   => array('.page-title'),
                        'default'  => '#000000',
                        'validate' => 'color',
                        'transparent' => false,
                        ),
                    array(
                        'id'=>'bpxl_nav_break',
                        'type' => 'info',
                        'desc' => __('Footer', 'bloompixel')
                        ),
                    array( 
                        'id'       => 'bpxl_footer_color',
                        'type'     => 'background',
                        'title'    => __('Footer Background Color', 'bloompixel'),
                        'subtitle' => __('Pick a background color for the footer.', 'bloompixel'),
                        'output'   => array('.footer, footer .widget-title span'),
                        'preview_media' => true,
                        'preview' => false,
                        'transparent' => false,
                        'background-image' => false,
                        'background-position' => false,
                        'background-repeat' => false,
                        'background-attachment' => false,
                        'background-size' => false,
                        'default' => array(
                                'background-color'  => '#ffffff', 
                            ),
                        ),
                    array(
                        'id'=>'bpxl_footer_link_color',
                        'type' => 'link_color',
                        'title' => __('Footer Link Color', 'bloompixel'), 
                        'subtitle' => __('Pick a link color for the footer.', 'bloompixel'),
                        'output'   => array('.footer a'),
                        'default'  => array(
                            'regular'  => '#000000',
                            'hover'    => '#ffbb33',
                        ),
                        'validate' => 'color',
                        'transparent' => false,
                        'active' => false,
                        ),
                    array(
                        'id'       => 'bpxl_footer_widget_title_color',
                        'type'     => 'color',
                        'title'    => __('Footer Widget Title Color', 'bloompixel'), 
                        'subtitle' => __('Pick a color for title of footer widgets.', 'bloompixel'),
                        'output'   => array('.footer-widget .widget-title'),
                        'default'  => '#000000',
                        'validate' => 'color',
                        'transparent' => false,
                        ),
                    array( 
                        'id'       => 'bpxl_credit_bg_color',
                        'type'     => 'background',
                        'title'    => __('Credit/Copyright Background Color', 'bloompixel'),
                        'subtitle' => __('Pick a background color for the credit or copyright area below footer.', 'bloompixel'),
                        'output'   => array('.copyright'),
                        'preview_media' => true,
                        'preview' => false,
                        'transparent' => false,
                        'background-image' => false,
                        'background-position' => false,
                        'background-repeat' => false,
                        'background-attachment' => false,
                        'background-size' => false,
                        'default' => array(
                                'background-color'  => '#ffffff',
                            ),
                        ),
                    array( 
                        'id'=>'bpxl_credit_color',
                        'type' => 'color',
                        'title'    => __('Credit/Copyright Text Color', 'bloompixel'),
                        'subtitle' => __('Pick a text color for the credit or copyright area below footer.', 'bloompixel'),
                        'default' => '#2e2e2e ',
                        'validate' => 'color',
                        'transparent' => false,
                        'output'    => array(
                                'color'            => '.copyright',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_custom_css',
                        'type' => 'ace_editor',
                        'title' => __('Custom CSS', 'bloompixel'), 
                        'subtitle' => __('Quickly add some CSS to your theme by adding it to this block.', 'bloompixel'),
                        'mode' => 'css',
                        'theme' => 'monokai',
                        'default' => ""
                        ),
                )
            );
            
            $this->sections[] = array(
                'icon' => 'el-icon-font',
                'icon_class' => 'icon-large',
                'title' => __('Typography', 'bloompixel'),
                'fields' => array(                                      
                    array(
                        'id'=>'bpxl_body_font',
                        'type' => 'typography',
                        'output' => array('body'),
                        'title' => __('Body Font', 'bloompixel'),
                        'subtitle' => __('Select the main body font for your theme.', 'bloompixel'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'font-size'=>false,
                        'line-height'=>false,
                        'default' => array(
                            'font-family'=>'Roboto',
                            'font-weight'=>'400',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_main_nav_font',
                        'type' => 'typography',
                        'output' => array('.menu a'),
                        'title' => __('Main Navigation Font', 'bloompixel'),
                        'subtitle' => __('Select the font for main navigation.', 'bloompixel'),
                        'google'=>true,
                        'color'=>false,
                        'text-align'=>false,
                        'line-height'=>false,
                        'default' => array(
                            'font-family'=>'Roboto',
                            'font-size'=>'12px',
                            'font-weight'=>'500',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_headings_font',
                        'type' => 'typography',
                        'output' => array('h1,h2,h3,h4,h5,h6, .header, .widgettitle, .article-heading, .ws-title, .social-widget a, .post-navigation, #wp-calendar caption, .fn, #commentform input, #commentform textarea, input[type="submit"], .pagination, .footer-subscribe'),
                        'title' => __('Headings Font', 'bloompixel'),
                        'subtitle' => __('Select the font for headings for your theme.', 'bloompixel'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'font-size'=>false,
                        'line-height'=>false,
                        'default' => array(
                            'font-family'=>'Merriweather',
                            'font-weight'=>'700',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_title_font',
                        'type' => 'typography',
                        'output' => array('.entry-title'),
                        'title' => __('Post Title Font', 'bloompixel'),
                        'subtitle' => __('Select the font for titles of posts for your theme.', 'bloompixel'),
                        'google'=>true,
                        'text-align'=>false,
                        'text-transform'=>true,
                        'color'=>false,
                        'default' => array(
                            'font-family'=>'Raleway',
                            'font-size'=>'42px',
                            'font-weight'=>'800',
                            'line-height'=>'45px',
                            'text-transform'=>'uppercase',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_single_title_font',
                        'type' => 'typography',
                        'output' => array('.single-title'),
                        'title' => __('Single Post Title Font', 'bloompixel'),
                        'subtitle' => __('Select the font for titles of posts for your theme.', 'bloompixel'),
                        'google'=>true,
                        'text-align'=>true,
                        'text-transform'=>true,
                        'color'=>false,
                        'default' => array(
                            'font-family'=>'Raleway',
                            'font-size'=>'64px',
                            'font-weight'=>'800',
                            'line-height'=>'70px',
                            'text-transform'=>'none',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_post_content_font',
                        'type' => 'typography',
                        'output' => array('.post-content'),
                        'title' => __('Post Content Font', 'bloompixel'),
                        'subtitle' => __('Select the font size and style for post content.', 'bloompixel'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'font-size'=>true,
                        'line-height'=>true,
                        'default' => array(
                            'font-family'=>'Merriweather',
                            'font-size'=>'16px',
                            'font-weight'=>'300',
                            'line-height'=>'32px',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_meta_font',
                        'type' => 'typography',
                        'output' => array('.post-meta, .meta, .r-meta, .post-cats'),
                        'title' => __('Meta Font', 'bloompixel'),
                        'subtitle' => __('Select the font size and style for meta section.', 'bloompixel'),
                        'google'=>true,
                        'text-align'=>true,
                        'color'=>false,
                        'text-transform'=>true,
                        'font-size'=>true,
                        'line-height'=>true,
                        'default' => array(
                            'font-family'=>'Roboto',
                            'font-size'=>'13px',
                            'font-weight'=>'400',
                            'line-height'=>'20px',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_sidebar_font',
                        'type' => 'typography',
                        'output' => array('.sidebar'),
                        'title' => __('Sidebar Font', 'bloompixel'),
                        'subtitle' => __('Select the font for sidebar.', 'bloompixel'),
                        'google'=>true,
                        'line-height'=>false,
                        'font-size'=>false,
                        'font-weight'=>false,
                        'font-style'=>false,
                        'text-align'=>false,
                        'color'=>false,
                        'default' => array(
                            'font-family'=>'Roboto',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_widget_title_font',
                        'type' => 'typography',
                        'output' => array('.widget-title, #tabs li, .section-heading, .comment-reply-link, .post-navigation .post-nav-links span'),
                        'title' => __('Widget Title Font', 'bloompixel'),
                        'subtitle' => __('Select the font for titles of widgets for your theme.', 'bloompixel'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'default' => array(
                            'font-family'=>'Roboto',
                            'font-size'=>'15px',
                            'font-weight'=>'700',
                            'line-height'=>'21px',
                            ),
                        ),
                    array(
                        'id'=>'bpxl_logo_font',
                        'type' => 'typography',
                        'output' => array('.header #logo a, .footer-logo-wrap'),
                        'title' => __('Logo Font', 'bloompixel'),
                        'subtitle' => __('Select the font for logo for your theme.', 'bloompixel'),
                        'google'=>true,
                        'text-align'=>false,
                        'color'=>false,
                        'font-size'=>true,
                        'line-height'=>true,
                        'text-transform'=>true,
                        'default' => array(
                            'font-family'=>'Montserrat',
                            'font-size'=>'28px',
                            'font-weight'=>'700',
                            'line-height'=>'38px'
                            ),
                        ),
                )
            );
            
            $this->sections[] = array(
                'icon' => 'el-icon-home',
                'icon_class' => 'icon-large',
                'title' => __('Home', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'=>'bpxl_home_break',
                        'type' => 'info',
                        'desc' => __('Home Content', 'bloompixel')
                        ), 
                    array(
                        'id'=>'bpxl_home_latest_posts',
                        'type' => 'switch', 
                        'title' => __('Show Latest Posts by Category', 'bloompixel'),
                        'subtitle'=> __('Choose this option to show latest posts by category on homepage.', 'bloompixel'),
                        "default"       => 0,
                        'on' => 'Yes',
                        'off' => 'No',
                        ),  
                    array(
                        'id'        => 'bpxl_home_latest_cat',
                        'type'     => 'select',
                        'multi'    => true,
                        'data' => 'categories',
                        'title'    => __('Latest Posts Category', 'bloompixel'), 
                        'required' => array('bpxl_home_latest_posts','=','1'),
                        'subtitle' => __('Select category/categories for latest posts on homepage. Use Ctrl key to select more than one category.', 'bloompixel'),
                        ),
                    array(
                        'id'=>'bpxl_home_content',
                        'type' => 'switch', 
                        'title' => __('Show Post Excerpts on Homepage', 'bloompixel'),
                        'subtitle'=> __('Choose this option to show or hide post excerpts on homepage.', 'bloompixel'),
                        "default"       => 1,
                        'on' => 'Yes',
                        'off' => 'No',
                        ), 
                    array(
                        'id'=>'bpxl_excerpt_length',
                        'type' => 'slider', 
                        'title' => __('Excerpt Length', 'bloompixel'), 
                        'required' => array('bpxl_home_content','=','1'),
                        'subtitle' => __('Paste the length of post excerpt to be shown on homepage.', 'bloompixel'),
                        "default"   => "40",
                        "min"       => "0",
                        "step"      => "1",
                        "max"       => "400",
                        ),
                    array(
                        'id'=>'bpxl_meta_break',
                        'type' => 'info',
                        'desc' => __('Post Meta', 'bloompixel')
                        ),
                    array(
                        'id'=>'bpxl_post_meta',
                        'type' => 'switch', 
                        'title' => __('Post Meta', 'bloompixel'),
                        'subtitle'=> __('Choose the option to show or hide post meta (Categories, Tags, Author Name etc) on homepage and archives.', 'bloompixel'),
                        "default"       => 1,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),  
                    array(
                        'id'=>'bpxl_post_meta_options',
                        'type' => 'checkbox',
                        'title' => __('Post Meta Info Options', 'bloompixel'), 
                        'required' => array('bpxl_post_meta','=','1'),    
                        'subtitle' => __('Select which items you want to show for post meta on homepage and archives.', 'bloompixel'),
                        'options' => array('1' => 'Post Author','2' => 'Date','3' => 'Post Category','4' => 'Time to Read' ),
                        'default' => array('1' => '1', '2' => '1', '3' => '1', '4' => '1', '5' => '1')
                        ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el-icon-check-empty',
                'icon_class' => 'icon-large',
                'title' => __('Header', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'=>'bpxl_header_style',
                        'type' => 'image_select',
                        'compiler'=>true,
                        'title' => __('Header Style', 'bloompixel'), 
                        'subtitle' => __('Select layout style for archives. <br><br><strong>Note:</strong> These layouts are applied to archives (Category, tags etc).', 'bloompixel'),
                        'options' => array(
                                '1' => array('alt' => 'Header 1', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/header-1.png'),
                                '2' => array('alt' => 'Header 2', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/header-2.png'),
                                '3' => array('alt' => 'Header 3', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/header-3.png'),
                            ),
                        'default' => 'gridlayout'
                        ),
                    array(
                        'id'=>'bpxl_tagline',
                        'type' => 'switch', 
                        'title' => __('Show Tagline', 'bloompixel'),
                        'subtitle'=> __('Choose the option to show or hide tagline below logo.', 'bloompixel'),
                        "default"       => 0,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),
                    array(
                        'id'=>'bpxl_header_search',
                        'type' => 'switch', 
                        'title' => __('Show Search Box', 'bloompixel'),
                        'subtitle'=> __('Choose the option to show or hide search box in navigation menu', 'bloompixel'),
                        "default"       => 0,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),
                    array(
                        'id'=>'bpxl_sticky_menu',
                        'type' => 'switch', 
                        'title' => __('Sticky Menu', 'bloompixel'),
                        'subtitle'=> __('Choose the option to enable or disable the sticky/floating menu.', 'bloompixel'),
                        "default"       => 0,
                        'on' => 'Enable',
                        'off' => 'Disable',
                        ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el-icon-minus',
                'icon_class' => 'icon-large',
                'title' => __('Footer', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'=>'bpxl_footer_widgets',
                        'type' => 'switch', 
                        'title' => __('Show Footer Widgets', 'bloompixel'),
                        'subtitle'=> __('Choose the option to show or hide footer widgets.', 'bloompixel'),
                        "default"       => 1,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),
                    array(
                        'id'=>'bpxl_footer_columns',
                        'type' => 'image_select',
                        'required' => array('bpxl_footer_widgets','=','1'),
                        'compiler'=>true,
                        'title' => __('Footer Columns', 'bloompixel'), 
                        'subtitle' => __('Select number of columns for footer. Choose between 1, 2, 3 or 4 columns.', 'bloompixel'),
                        'options' => array(
                                'footer_4' => array('alt' => '4 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/f4c.png'),
                                'footer_3' => array('alt' => '3 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/f3c.png'),
                                'footer_2' => array('alt' => '2 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/f2c.png'),
                                'footer_1' => array('alt' => '1 Column', 'img' => get_template_directory_uri() .'/inc/options/settings/img/layouts/f1c.png'),
                            ),
                        'default' => 'footer_4'
                        ),
                    array(
                        'id'=>'bpxl_footer_text',
                        'type' => 'editor',
                        'title' => __('Copyright Text', 'bloompixel'), 
                        'subtitle' => __('Enter copyright text to be shown on footer or you can keep it blank to show nothing.', 'bloompixel'),
                        'default' => '&copy; Copyright 2015. Theme by <a href="http://themeforest.net/user/BloomPixel/portfolio?ref=bloompixel">BloomPixel</a>.',
                        'editor_options'   => array(
                                'media_buttons'    => false,
                                'textarea_rows'    => 5
                            )
                        ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el-icon-folder-open',
                'icon_class' => 'icon-large',
                'title' => __('Single Post Options', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'      => 'bpxl_single_post_layout',
                        'type'    => 'sorter',
                        'title'   => 'Single Posts Layout',
                        'subtitle'    => 'Organize the elements of single posts.',
                        'options' => array(
                            'enabled'  => array(
                                'post-content'    => 'Post Content',
                                'post-navigation' => 'Post Navigation',
                                'author-box'      => 'Author Box',
                                'related-posts'   => 'Related Posts'
                            ),
                            'disabled' => array(
                            )
                        ),
                    ),
                    array(
                        'id'=>'bpxl_breadcrumbs',
                        'type' => 'switch', 
                        'title' => __('Breadcrumbs', 'bloompixel'),
                        'subtitle'=> __('Choose the option to show or hide breadcrumbs on single posts.', 'bloompixel'),
                        "default"       => 1,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),
					array(
						'id'       => 'bpxl_breadcrumbs_type',
						'type'     => 'select',
						'title'    => __('Breadcrumbs Type', 'bloompixel'),
						'subtitle' => __('Select type of breadcrumbs that you want to use. <br/><strong>Note:</strong> To use Yoast\'s breadcrumbs, you need to install WordPress SEO plugin by Yoast.', 'bloompixel'),
						'required' => array('bpxl_breadcrumbs','=','1'),
						'options'  => array(
								'theme_breadcrumb' => 'Theme\'s built-in Breadcrumbs',
								'yoast_breadcrumb' => 'Yoast\'s Breadcrumbs',
							),
						'default'  => 'theme_breadcrumb',
						),
                    array(
                        'id'=>'bpxl_single_featured',
                        'type' => 'switch', 
                        'title' => __('Show Featured Content', 'bloompixel'),
                        'subtitle'=> __('Choose the option to show or hide featured thumbnails, gallery, audio or video on single posts.', 'bloompixel'),
                        "default"       => 1,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),
                    array(
                        'id'=>'bpxl_single_meta_break',
                        'type' => 'info',
                        'desc' => __('Post Meta', 'bloompixel')
                        ),
                    array(
                        'id'=>'bpxl_single_meta',
                        'type' => 'switch', 
                        'title' => __('Single Post Meta', 'bloompixel'),
                        'subtitle'=> __('Choose the option to show or hide post meta (Categories, Tags, Author Name etc) on single posts.', 'bloompixel'),
                        "default"       => 1,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),  
                    array(
                        'id'=>'bpxl_single_post_meta_options',
                        'type' => 'checkbox',
                        'title' => __('Single Post Meta Info Options', 'bloompixel'), 
                        'required' => array('bpxl_single_meta','=','1'),
                        'subtitle' => __('Select which items you want to show for post meta on single pages.', 'bloompixel'),
                        'options' => array('1' => 'Post Author','2' => 'Date','3' => 'Post Category','4' => 'Post Tags','5' => 'Post Read Time','6' => 'Post Comments','7' => 'Subscribe Button'),//Must provide key => value pairs for multi checkbox options
                        'default' => array('1' => '1', '2' => '1', '3' => '1', '4' => '1', '5' => '1', '6' => '1', '7' => '1')//See how std has changed? you also don't need to specify opts that are 0.
                        ),
                    array(
                        'id'=>'bpxl_related_break',
                        'type' => 'info',
                        'desc' => __('Related Posts', 'bloompixel')
                        ),
                    array(
                        'id'=>'bpxl_related_posts_count',
                        'type' => 'slider', 
                        'title' => __('Number of Related Posts', 'bloompixel'),
                        'subtitle'=> __('Choose the number of related posts you want to show.', 'bloompixel'),
                        "default"   => "3",
                        "min"       => "3",
                        "step"      => "1",
                        "max"       => "20",
                        ),  
                    array(
                        'id'=>'bpxl_related_posts_by',
                        'type' => 'radio',
                        'title' => __('Filter Related Posts By', 'bloompixel'), 
                        'subtitle' => __('Choose whether to show related posts by categories or tags.', 'bloompixel'),
                        'options' => array('1' => 'Categories', '2' => 'Tags'),//Must provide key => value pairs for radio options
                        'default' => '1',
                        'customizer' => false
                        ),
                    array(
                        'id'=>'bpxl_single_share_break',
                        'type' => 'info',
                        'desc' => __('Sharing Buttons', 'bloompixel')
                        ),
                    array(
                        'id'=>'bpxl_show_share_buttons',
                        'type' => 'switch', 
                        'title' => __('Social Media Share Buttons', 'bloompixel'),
                        'subtitle'=> __('Choose the option to show or hide social media share buttons.', 'bloompixel'),
                        "default"       => 0,
                        'on' => 'Show',
                        'off' => 'Hide',
                        ),
                    array(
                        'id'=>'bpxl_share_buttons',
                        'type'     => 'sortable',
                        'title' => __('Select Share Buttons', 'bloompixel'), 
                        'required' => array('bpxl_show_share_buttons','=','1'), 
                        'subtitle' => __('Select which buttons you want to show. You can drag and drop the buttons to change the position.', 'bloompixel'),
                        'mode'     => 'checkbox',
                        'options'  => array(
                            'fb'     => 'Facebook',
                            'twitter'    => 'Twitter',
                            'gplus'  => 'Google+',
                            'pinterest'  => 'Pinterest',
                            'stumbleupon'  => 'StumbleUpon',
                            'reddit'  => 'Reddit',
                            'linkedin'  => 'Linkedin',
                            'tumblr'  => 'tumblr',
                            'email'  => 'Email',
                        ),
                        // For checkbox mode
                        'default' => array(
                            'fb' => true,
                            'twitter' => true,
                            'gplus' => true,
                            'pinterest' => false,
                            'stumbleupon' => false,
                            'reddit' => false,
                            'linkedin' => false,
                            'tumblr' => false,
                            'email' => false,
                        ),
                    ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el-icon-eur',
                'icon_class' => 'icon-large',
                'title' => __('Ad Management', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'=>'bpxl_header_ad',
                        'type' => 'textarea',
                        'title' => __('Header Ad', 'bloompixel'), 
                        'subtitle' => __('Paste your ad code here.<br/><br/> This ad only appears when Header Style 3 is selected.', 'bloompixel'),
                        'default' => ''
                        ),
                    array(
                        'id'=>'bpxl_below_title_ad',
                        'type' => 'textarea',
                        'title' => __('Below Post Title Ad', 'bloompixel'), 
                        'subtitle' => __('Paste your ad code here.', 'bloompixel'),
                        'default' => ''
                        ),
                    array(
                        'id'=>'bpxl_below_content_ad',
                        'type' => 'textarea',
                        'title' => __('Below Post Content Ad', 'bloompixel'), 
                        'subtitle' => __('Paste your ad code here.', 'bloompixel'),
                        'default' => ''
                        ),
                )
            );

            $this->sections[] = array(
                'icon' => 'el-icon-check',
                'icon_class' => 'icon-large',
                'title' => __('Updates', 'bloompixel'),
                'fields' => array(
                    array(
                        'id'=>'bpxl_envato_user_name',
                        'type' => 'text',
                        'title' => __('Envato Username', 'bloompixel'), 
                        'subtitle' => __('Enter your Envato (ThemeForest) username here.', 'bloompixel'),
                        'default' => ""
                        ),
                    array(
                        'id'=>'bpxl_envato_api_key',
                        'type' => 'text',
                        'title' => __('Envato API Key', 'bloompixel'), 
                        'subtitle' => __('Enter your Envato API key here.', 'bloompixel'),
                        'default' => ""
                        ),
                )
            );  

                $this->sections[] = array(
                    'title'  => __( 'Import / Export', 'redux-framework-demo' ),
                    'desc'   => __( 'Import and Export your Redux Framework settings from file, text or URL.', 'redux-framework-demo' ),
                    'icon'   => 'el el-icon-refresh',
                    'fields' => array(
                        array(
                            'id'         => 'opt-import-export',
                            'type'       => 'import_export',
                            'title'      => 'Import Export',
                            'subtitle'   => 'Save and restore your Redux options',
                            'full_width' => false,
                        ),
                    ),
                );
            }

            public function setHelpTabs() {

                // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-1',
                    'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
                    'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
                );

                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-2',
                    'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
                    'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
                );

                // Set the help sidebar
                $this->args['help_sidebar'] = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
            }

            /**
             * All the possible arguments for Redux.
             * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
             * */
            public function setArguments() {

                $theme = wp_get_theme(); // For use with some settings. Not necessary.

                $this->args = array(
                    // TYPICAL -> Change these values as you need/desire
                    'opt_name'             => 'bpxl_story_options',
                    // This is where your data is stored in the database and also becomes your global variable name.
                    'display_name'         => $theme->get( 'Name' ),
                    // Name that appears at the top of your panel
                    'display_version'      => $theme->get( 'Version' ),
                    // Version that appears at the top of your panel
                    'menu_type'            => 'menu',
                    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                    'allow_sub_menu'       => true,
                    // Show the sections below the admin menu item or not
                    'menu_title'        => __('Theme Options', 'bloompixel'),
                    'page_title'        => __('Theme Options', 'bloompixel'),
                    // You will need to generate a Google API key to use this feature.
                    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                    'google_api_key'       => 'AIzaSyAX_2L_UzCDPEnAHTG7zhESRVpMPS4ssII',
                    // Set it you want google fonts to update weekly. A google_api_key value is required.
                    'google_update_weekly' => false,
                    // Must be defined to add google fonts to the typography module
                    'async_typography'     => true,
                    // Use a asynchronous font on the front end or font string
                    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                    'admin_bar'            => true,
                    // Show the panel pages on the admin bar
                    'admin_bar_icon'     => 'dashicons-portfolio',
                    // Choose an icon for the admin bar menu
                    'admin_bar_priority' => 50,
                    // Choose an priority for the admin bar menu
                    'global_variable'      => '',
                    // Set a different name for your global variable other than the opt_name
                    'dev_mode'             => false,
                    // Show the time the page took to load, etc
                    'update_notice'        => true,
                    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
                    'customizer'           => true,
                    // Enable basic customizer support
                    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
                    'ajax_save'          =>true,

                    // OPTIONAL -> Give you extra features
                    'page_priority'        => null,
                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                    'page_parent'          => 'themes.php',
                    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                    'page_permissions'     => 'manage_options',
                    // Permissions needed to access the options panel.
                    'menu_icon'            => '',
                    // Specify a custom URL to an icon
                    'last_tab'             => '',
                    // Force your panel to always open to a specific tab (by id)
                    'page_icon'            => 'icon-themes',
                    // Icon displayed in the admin panel next to your menu_title
                    'page_slug'            => '_options',
                    // Page slug used to denote the panel
                    'save_defaults'        => true,
                    // On load save the defaults to DB before user clicks save or not
                    'default_show'         => false,
                    // If true, shows the default value next to each field that is not the default value.
                    'default_mark'         => '',
                    // What to print by the field's title if the value shown is default. Suggested: *
                    'show_import_export'   => true,
                    // Shows the Import/Export panel when not used as a field.

                    // CAREFUL -> These options are for advanced use only
                    'transient_time'       => 60 * MINUTE_IN_SECONDS,
                    'output'               => true,
                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                    'output_tag'           => true,
                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                    'database'             => '',
                    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                    'system_info'          => false,
                    // REMOVE

                    // HINTS
                    'hints'                => array(
                        'icon'          => 'el el-question-sign',
                        'icon_position' => 'right',
                        'icon_color'    => 'lightgray',
                        'icon_size'     => 'normal',
                        'tip_style'     => array(
                            'color'   => 'light',
                            'shadow'  => true,
                            'rounded' => false,
                            'style'   => '',
                        ),
                        'tip_position'  => array(
                            'my' => 'top left',
                            'at' => 'bottom right',
                        ),
                        'tip_effect'    => array(
                            'show' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'mouseover',
                            ),
                            'hide' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'click mouseleave',
                            ),
                        ),
                    )
                );

                // Panel Intro text -> before the form
                if ( ! isset( $this->args['global_variable'] ) || $this->args['global_variable'] !== false ) {
                    if ( ! empty( $this->args['global_variable'] ) ) {
                        $v = $this->args['global_variable'];
                    } else {
                        $v = str_replace( '-', '_', $this->args['opt_name'] );
                    }
                    //$this->args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
                } else {
                    $this->args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
                }

                // Add content after the form.
                //$this->args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );
            }

            public function validate_callback_function( $field, $value, $existing_value ) {
                $error = true;
                $value = 'just testing';

                /*
              do your validation

              if(something) {
                $value = $value;
              } elseif(something else) {
                $error = true;
                $value = $existing_value;
                
              }
             */

                $return['value'] = $value;
                $field['msg']    = 'your custom error message';
                if ( $error == true ) {
                    $return['error'] = $field;
                }

                return $return;
            }

            public static function class_field_callback( $field, $value ) {
                print_r( $field );
                echo '<br/>CLASS CALLBACK';
                print_r( $value );
            }

        }

        global $reduxConfig;
        $reduxConfig = new Redux_Framework_sample_config();
    } else {
        echo "The class named Redux_Framework_sample_config has already been called. <strong>Developers, you need to prefix this class with your company name or you'll run into problems!</strong>";
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ):
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    endif;

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ):
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error = true;
            $value = 'just testing';

            /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            
          }
         */

            $return['value'] = $value;
            $field['msg']    = 'your custom error message';
            if ( $error == true ) {
                $return['error'] = $field;
            }

            $return['warning'] = $field;

            return $return;
        }
    endif;
