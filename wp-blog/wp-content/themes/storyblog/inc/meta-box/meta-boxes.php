<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */


add_filter( 'rwmb_meta_boxes', 'bpxl_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function bpxl_register_meta_boxes( $meta_boxes )
{
	/**
	 * Prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'bpxl_';
	
	$meta_boxes[] = array(
		'title' => __( 'Post Options', 'textdomain' ),
		'pages' => array( 'post', 'page' ),
		'fields' => array(
			// Cover Image
			array(
				'name' => __( 'Show Cover Image?', 'rwmb' ),
				'id'   => "{$prefix}post_cover_show",
				'type' => 'checkbox',
				'desc'  => __( 'Check this option to show cover image on this post.', 'rwmb' ),
				// Value can be 0 or 1
				'std'  => 0,
			),
		),
	);
	
	$meta_boxes[] = array(
		'title' => __( 'Background Options', 'textdomain' ),
		'pages' => array( 'post', 'page' ),
		'fields' => array(
			// BACKGROUD COLOR
			array(
				'name' => '<strong>' . __( 'Background Color', 'rwmb' ) . '</strong>',
				'id'   => "{$prefix}post_bg_color",
				'type' => 'color',
			),
			// BACKGROUD OPACITY
			array(
				'name' => '<strong>' . __( 'Background Opacity', 'rwmb' ) . '</strong><br>' . __( 'Choose a value between 0 to 100.', 'rwmb' ),
				'id'   => "{$prefix}post_opacity",
				'type' => 'number',
    			'std'  => '60',
    			'step' => '5',
    			'min'  => '0',
			),
		),
	);
	
	$meta_boxes[] = array(
		'title' => __( 'Layout Options', 'textdomain' ),
		'pages' => array( 'post', 'page' ),
		'fields' => array(
			array(
				'id'       => "{$prefix}layout",
				'name'     => __( 'Layout', 'rwmb' ),
				'type'     => 'image_select',

				// Array of 'value' => 'Image Source' pairs
				'options'  => array(
					'default'  => get_template_directory_uri() . '/inc/meta-box/img/default.png',
					'cslayout'  => get_template_directory_uri() . '/inc/meta-box/img/right.png',
					'sclayout' => get_template_directory_uri() . '/inc/meta-box/img/left.png',
					'flayout'  => get_template_directory_uri() . '/inc/meta-box/img/none.png',
				),

				// Allow to select multiple values? Default is false
				// 'multiple' => true,
			),
		),
	);

	return $meta_boxes;
}


