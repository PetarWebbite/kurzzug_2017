<?php
/**
 * Custom template tags for StoryBlog
 *
 * @package WordPress
 * @subpackage storyblog
 * @since StoryBlog 1.0
 */

/*-----------------------------------------------------------------------------------*/
/*	Post and Header Classes
/*-----------------------------------------------------------------------------------*/

if ( ! function_exists( 'bpxl_layout_class' ) ) :
	function bpxl_layout_class() {
		global $bpxl_story_options;
		
		$bpxl_class = '';
		
		if( is_home() || is_front_page() || is_search() || is_404() ) {
            $bpxl_class = $bpxl_story_options['bpxl_layout'];
		}
		elseif(is_archive() || is_author()) {
            $bpxl_class = $bpxl_story_options['bpxl_archive_layout'];
		}
		elseif( is_single() ) {
            $sidebar_positions = rwmb_meta( 'bpxl_layout', $args = array('type' => 'image_select'), get_the_ID() );
		
            if( !empty($sidebar_positions) ) {
                $sidebar_position = $sidebar_positions;
                if ($sidebar_position == 'cslayout' || $sidebar_position == 'sclayout' || $sidebar_position == 'flayout' ) {
                    $bpxl_class = $sidebar_position;
                } else {
                    $bpxl_class = $bpxl_story_options['bpxl_single_layout'];
                }
            } else {
                $bpxl_class = $bpxl_story_options['bpxl_single_layout'];
            }
		}
		
		echo $bpxl_class;
	}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Add Span tag Around Categories and Archives Post Count
/*-----------------------------------------------------------------------------------*/
if(!function_exists('bpxl_cat_count')){ 
	function bpxl_cat_count($links) {
		return str_replace(array('</a> (',')'), array('<span class="cat-count">','</span></a>'), $links);
	}
}
add_filter('wp_list_categories', 'bpxl_cat_count');

if(!function_exists('bpxl_archive_count')){ 
	function bpxl_archive_count($links) {
	  	return str_replace(array('</a>&nbsp;(',')'), array('<span class="cat-count">','</span></a>'), $links);
	}
}
add_filter('get_archives_link', 'bpxl_archive_count');

/*-----------------------------------------------------------------------------------*/
/*	Exceprt Length
/*-----------------------------------------------------------------------------------*/

// Limit the Length of Excerpt
function bpxl_excerpt_length( $length ) {
	global $bpxl_story_options;
	if ( $bpxl_story_options['bpxl_excerpt_length'] != '') {
		$excerpt_length = $bpxl_story_options['bpxl_excerpt_length'];		
	} else {
		$excerpt_length = '40';
	}
	
	return $excerpt_length;
}
add_filter( 'excerpt_length', 'bpxl_excerpt_length', 999 );

// Remove […] string
function bpxl_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'bpxl_excerpt_more');

// Add Shortcodes in Excerpt Field
add_filter( 'get_the_excerpt', 'do_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Modify <!--more--> Tag in Posts
/*-----------------------------------------------------------------------------------*/
// Prevent Page Scroll When Clicking the More Link
function remove_more_link_scroll( $link ) {
	$link = preg_replace( '|#more-[0-9]+|', '', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );

/*-----------------------------------------------------------------------------------*/
/*	Pagination
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'bpxl_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Travelista 1.0
 */
function bpxl_paging_nav() {
	global $bpxl_story_options;
	
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.
	$links = paginate_links( array(
		'base'     => $pagenum_link,
		'format'   => $format,
		'total'    => $GLOBALS['wp_query']->max_num_pages,
		'current'  => $paged,
		'mid_size' => 1,
		'add_args' => array_map( 'urlencode', $query_args ),
		'prev_text' => __( '&larr; Previous', 'bloompixel' ),
		'next_text' => __( 'Next &rarr;', 'bloompixel' ),
	) );
	if ($bpxl_story_options['bpxl_pagination_type'] == '1') :
		if ( $links ) :

		?>
		<nav class="navigation paging-navigation" role="navigation">
			<div class="pagination loop-pagination">
				<?php echo $links; ?>
			</div><!-- .pagination -->
		</nav><!-- .navigation -->
		<?php
		endif;
	else:
	?>
		<nav class="norm-pagination" role="navigation">
			<div class="nav-previous"><?php next_posts_link( '<i class="fa fa-chevron-left"></i>
 ' . __( 'Older posts', 'bloompixel' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts', 'bloompixel' ).' <i class="fa fa-chevron-right"></i>' ); ?></div>
		</nav>
	<?php
	endif;
}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Post Navigation
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'bpxl_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 *
 * @since Travelista 1.0
 */
function bpxl_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}

	?>
	<nav class="navigation post-navigation textcenter clearfix" role="navigation">
		<?php
		if ( is_attachment() ) :
			next_post_link('<div class="alignleft post-nav-links prev-link-wrapper"><div class="next-link"><span class="uppercase">'. __("Published In","bloompixel") .'</span> %link'."</div></div>");
		else :

			$prev_post_bg = '';
			$next_post_bg = '';

			$prev_post = get_previous_post();
			if (!empty( $prev_post )):
				$prev_post_bg = get_the_post_thumbnail( $prev_post->ID, 'featured370' );
			endif;
			
			$next_post = get_next_post();
			if (!empty( $next_post )):
				$next_post_bg = get_the_post_thumbnail( $next_post->ID, 'featured370' );
			endif;

			previous_post_link('<div class="alignleft post-nav-links prev-link-wrapper"><div class="post-nav-link-bg">'. $prev_post_bg .'</div><div class="prev-link"><span class="uppercase"><i class="fa fa-long-arrow-left"></i> &nbsp;'. __("Previous Article","bloompixel").'</span> %link'."</div></div>");
			next_post_link('<div class="alignright post-nav-links next-link-wrapper"><div class="post-nav-link-bg">'. $next_post_bg .'</div><div class="next-link"><span class="uppercase">'. __("Next Article","bloompixel") .' &nbsp;<i class="fa fa-long-arrow-right"></i></span> %link'."</div></div>");
		endif;
		?>
	</nav><!-- .navigation -->
	<?php
}
endif;


/*-----------------------------------------------------------------------------------*/
/*	Estimate time required to read the article
/*-----------------------------------------------------------------------------------*/
function bpxl_estimated_reading_time() {

    $post = get_post();

    $words = str_word_count( strip_tags( $post->post_content ) );
    $minutes = floor( $words / 120 );
    $seconds = floor( $words % 120 / ( 120 / 60 ) );

    if ( 1 <= $minutes ) {
        if($seconds >= 30) {
            $estimated_time = $minutes + 1 . ' ' . __('Minute Read','bloompixel');
        } else {
            $estimated_time = $minutes . ' ' . __('Minute','bloompixel') . ($minutes == 1 ? '' : 's') . ' ' . __('Read','bloompixel');
        }
    } else {
        $estimated_time = $seconds . ' ' . __('Second','bloompixel') . ($seconds == 1 ? '' : 's') . ' ' . __('Read','bloompixel');
    }

    return $estimated_time;

}

/*-----------------------------------------------------------------------------------*/
/*	Add Extra Fields to User Profiles
/*-----------------------------------------------------------------------------------*/
add_action( 'show_user_profile', 'bpxl_user_profile_fields' );
add_action( 'edit_user_profile', 'bpxl_user_profile_fields' );

function bpxl_user_profile_fields( $user ) { ?>

<h3><?php _e("Social Profiles", "bloompixel"); ?></h3>

<table class="form-table">
	<tr>
		<th><label for="facebook"><?php _e("Facebook","bloompixel"); ?></label></th>
		<td>
		<input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php _e("Enter your facebook profile URL.","bloompixel"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="twitter"><?php _e("Twitter","bloompixel"); ?></label></th>
		<td>
		<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php _e("Enter your twitter profile URL.","bloompixel"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="googleplus"><?php _e("Google+","bloompixel"); ?></label></th>
		<td>
		<input type="text" name="googleplus" id="googleplus" value="<?php echo esc_attr( get_the_author_meta( 'googleplus', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php _e("Enter your Google+ profile URL.","bloompixel"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="linkedin"><?php _e("LinkedIn","bloompixel"); ?></label></th>
		<td>
		<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php _e("Enter your LinkedIn profile URL.","bloompixel"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="pinterest"><?php _e("Pinterest","bloompixel"); ?></label></th>
		<td>
		<input type="text" name="pinterest" id="pinterest" value="<?php echo esc_attr( get_the_author_meta( 'pinterest', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php _e("Enter your Pinterest profile URL.","bloompixel"); ?></span>
		</td>
	</tr>
	<tr>
		<th><label for="dribbble"><?php _e("Dribbble","bloompixel"); ?></label></th>
		<td>
		<input type="text" name="dribbble" id="dribbble" value="<?php echo esc_attr( get_the_author_meta( 'dribbble', $user->ID ) ); ?>" class="regular-text" /><br />
		<span class="description"><?php _e("Enter your Dribbble profile URL.","bloompixel"); ?></span>
		</td>
	</tr>
</table>
<?php }

add_action( 'personal_options_update', 'save_bpxl_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_bpxl_user_profile_fields' );

function save_bpxl_user_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

	update_user_meta( $user_id, 'author-attachment-url', $_POST['author-attachment-url'] );
	update_user_meta( $user_id, 'author-loc', $_POST['author-loc'] );
	update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
	update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
	update_user_meta( $user_id, 'googleplus', $_POST['googleplus'] );
	update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
	update_user_meta( $user_id, 'pinterest', $_POST['pinterest'] );
	update_user_meta( $user_id, 'dribbble', $_POST['dribbble'] );
}

function load_wp_media_files() {
  wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'load_wp_media_files' );
