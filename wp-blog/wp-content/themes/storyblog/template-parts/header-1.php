<?php global $bpxl_story_options; ?>
<header class="main-header header-1 clearfix">
    <div class="main-navigation">
        <div class="center-width">
            <div class="main-nav nav-down clearfix">
			
                <nav class="nav-menu" >
                    <div class="nav-menu-btn"><i class="fa fa-align-justify"></i> <?php _e( 'Menu', 'bloompixel'); ?></div>
					
                    <?php if ( has_nav_menu( 'main-menu' ) ) {
                        wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_class' => 'menu', 'container' => '', 'walker' => new bpxl_nav_walker ) );
                    } ?>
                    <?php if ( is_single() ) {
                        $bpxl_single_layout = rwmb_meta( 'bpxl_layout', $args = array('type' => 'image_select'), get_the_ID() );
		
                        if( !empty($bpxl_single_layout) ) {
                            if ( $bpxl_single_layout == 'cslayout' || $bpxl_single_layout == 'sclayout' ) {

                            } else { ?>
                                <div class="menu-btn off-menu fa fa-align-justify"></div>
                            <?php }
                        } else if( $bpxl_story_options['bpxl_single_layout'] != 'flayout' ) { ?>
                            <div class="menu-btn off-menu fa fa-align-justify"></div>
                        <?php } else { ?>
                            <div class="menu-btn off-menu fa fa-align-justify"></div>
                        <?php }
                    } else { ?>
                        <div class="menu-btn off-menu fa fa-align-justify"></div>
                    <?php } ?>
                    <?php if ($bpxl_story_options['bpxl_header_search'] == '1') { ?>
                        <div class="header-search">
                            <i class="fa fa-search"></i>
                            <?php get_search_form(); ?>
                        </div>
                    <?php } ?>
                </nav>
            </div><!-- .main-nav -->
        </div>
		<a href="http://shop.kurzzug.de/" id="main_link">hier geht's zurück zum Shop<a>
    </div><!-- .main-navigation -->
	<div class="header clearfix">
		<div class="container">
			<div class="logo-wrap">
			
				<?php if (!empty($bpxl_story_options['bpxl_logo']['url'])) { ?>
					<div id="logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<img src="<?php echo esc_url( $bpxl_story_options['bpxl_logo']['url'] ); ?>" <?php if (!empty($bpxl_story_options['bpxl_retina_logo']['url'])) { echo 'data-at2x="'. esc_url( $bpxl_story_options['bpxl_retina_logo']['url'] ) .'"';} ?> alt="<?php bloginfo( 'name' ); ?>">
						</a>
					</div>
				<?php } else { ?>
					<?php if( is_front_page() || is_home() || is_404() ) { ?>
						<h1 id="logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
						</h1>
					<?php } else { ?>
						<h2 id="logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
						</h2>
					<?php } ?>
				<?php } ?>
				<?php if ($bpxl_story_options['bpxl_tagline'] == '1') { ?>
					<span class="tagline">
						<?php bloginfo( 'description' ); ?>
					</span>
				<?php } ?>
			</div><!--.logo-wrap-->
		</div><!-- .container -->
	</div><!-- .header -->
</header>