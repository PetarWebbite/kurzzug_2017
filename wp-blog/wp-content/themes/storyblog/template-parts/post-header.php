<?php global $bpxl_story_options; ?>
<header>
    <h2 class="title entry-title">
        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
    </h2>
</header><!--.header-->