<?php
    global $bpxl_story_options;

    $orig_post = $post;
    global $post;

    //Related Posts By Categories
    if ($bpxl_story_options['bpxl_related_posts_by'] == '1') {
        $categories = get_the_category($post->ID);
        if ($categories) {
            $category_ids = array();
            foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
            $args=array(
                'category__in' => $category_ids,
                'post__not_in' => array($post->ID),
                'posts_per_page'=> $bpxl_story_options['bpxl_related_posts_count'], // Number of related posts that will be shown.
                'ignore_sticky_posts'=>1
            );
        }
    }
    //Related Posts By Tags
    else {
        $tags = wp_get_post_tags($post->ID);        
        if ($tags) {
            $tag_ids = array();  
            foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;  
            $args=array(
                'tag__in' => $tag_ids,  
                'post__not_in' => array($post->ID),  
                'posts_per_page'=> $bpxl_story_options['bpxl_related_posts_count'], // Number of related posts to display.  
                'ignore_sticky_posts'=>1 
            ); 
        }
    }
    $my_query = new wp_query( $args );
    if( $my_query->have_posts() ) {
        echo '<div class="related-posts single-box clearfix"><h3 class="section-heading uppercase"><span>' . __('Related Posts','bloompixel') . '</span></h3><div class="rposts-wrap">';
        while( $my_query->have_posts() ) {
            $my_query->the_post();?>
            <div class="post-grid">
                    <?php if ( has_post_thumbnail() ) {
                        $bpxl_post_coverurl = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <div class="post-cover" style="background-image: url(<?php echo esc_url( $bpxl_post_coverurl ); ?>);"></div>
                    <?php } ?>
                    <?php
                        if($bpxl_story_options['bpxl_post_meta'] == '1') { ?>
                            <div class="post-meta-thumb">
                                <?php if($bpxl_story_options['bpxl_post_meta_options']['1'] == '1') { ?>
                                    <div class="post-avtar">
                                        <?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '50' );  } ?>
                                        <span class="post-author"><?php _e( 'by','bloompixel'); ?> <?php the_author_posts_link(); ?></span>
                                    </div>
                                <?php } ?>
                                <?php
                                if($bpxl_story_options['bpxl_post_meta_options']['2'] == '1') { ?>
                                    <span class="posted-on">
                                        <time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>" title="<?php the_time('F j, Y'); ?>">
                                            <span class="post-day"><?php the_time('d'); ?></span>
                                            <span class="post-month"><?php the_time('F'); ?></span>
                                            <span class="post-year"><?php //the_time('Y'); ?></span>
                                        </time>
                                    </span><?php
                                } ?>
                            </div><?php
                        } ?>
                    <div class="post-inner">
            <?php
            if($bpxl_story_options['bpxl_post_meta_options']['3'] == '1') { ?>
                <div class="post-cats uppercase">
                    <?php $category = get_the_category();
                    if ($category) {
                      echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", "bloompixel" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
                    } ?>
                </div><?php
            } ?>
            <?php get_template_part('template-parts/post-header'); ?>
			
            <div class='read-time'><?php echo esc_attr( bpxl_estimated_reading_time() ); ?></div>
            <?php edit_post_link( __( 'Edit', 'bloompixel' ), '<div class="edit-link"><i class="fa fa-pencil-square-o"></i> ', '</div>' ); ?>
		</div><!--.post-inner-->
            </div>
            <?php
        }
        echo '</div></div>';
    }
    $post = $orig_post;
    wp_reset_query();
?>