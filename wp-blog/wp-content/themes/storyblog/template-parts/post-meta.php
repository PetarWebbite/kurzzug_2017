<?php global $bpxl_story_options; ?>
<div class="post-meta">
	<?php
		if ( is_single() ) {
            if($bpxl_story_options['bpxl_single_post_meta_options']['1'] == '1') { ?>
				<span class="post-author"><i class="fa fa-user"></i><?php the_author_posts_link(); ?></span>
			<?php } ?>
			<?php if($bpxl_story_options['bpxl_single_post_meta_options']['4'] == '1') { ?>
				<?php the_tags('<span class="post-tags"><i class="fa fa-tag"></i> ', ', ', '</span>'); ?>
			<?php } ?>
			<?php if($bpxl_story_options['bpxl_single_post_meta_options']['5'] == '1') { ?>
				<span class="post-comments"><i class="fa fa-comments-o"></i> <?php comments_popup_link( __( 'Leave a Comment', 'bloompixel' ), __( '1 Comment', 'bloompixel' ), __( '% Comments', 'bloompixel' ), 'comments-link', __( 'Comments are off', 'bloompixel' )); ?></span>
			<?php } ?>
			<?php edit_post_link( __( 'Edit', 'bloompixel' ), '<span class="edit-link"><i class="fa fa-pencil-square-o"></i> ', '</span>' );
		} else { ?>
			<?php edit_post_link( __( 'Edit', 'bloompixel' ), '<span class="edit-link"><i class="fa fa-pencil-square-o"></i> ', '</span>' );
		}
	?>
</div><!--.post-meta-->