<?php global $bpxl_story_options; ?>
<?php $bpxl_cover = rwmb_meta( 'bpxl_post_cover_show', $args = array('type' => 'checkbox'), $post->ID ); ?>
<article <?php post_class(); ?>>
    <?php if($bpxl_cover == '0' || $bpxl_cover == '' || $bpxl_story_options['bpxl_single_featured'] == '0' ) { ?>
        <header>
            <?php
            if($bpxl_story_options['bpxl_single_meta'] == '1') {
                if($bpxl_story_options['bpxl_single_post_meta_options']['3'] == '1') { ?>
                    <div class="post-cats uppercase">
                        <span>
                        <?php $category = get_the_category();
                        if ($category) {
                          echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", "bloompixel" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
                        } ?>
                        </span>
                    </div><?php
                }
            }
        ?>
        <h1 class="title single-title"><?php the_title(); ?></h1>
        <?php if( $bpxl_story_options['bpxl_single_meta'] == '1' ) { ?>
            <div class="post-meta">
                <?php
                    if($bpxl_story_options['bpxl_single_post_meta_options']['5'] == '1') { ?>
                        <span class='read-time'><?php echo esc_attr( bpxl_estimated_reading_time() ); ?></span>
                    <?php }
                    if($bpxl_story_options['bpxl_single_post_meta_options']['6'] == '1') { ?>
                        <span class="post-comments"><?php comments_popup_link( __( 'Leave a Comment', 'bloompixel' ), __( '1 Comment', 'bloompixel' ), __( '% Comments', 'bloompixel' ), 'comments-link', __( 'Comments are off', 'bloompixel' )); ?></span>
                    <?php }
                    edit_post_link( __( 'Edit', 'bloompixel' ), '<span class="edit-link"><i class="fa fa-pencil-square-o"></i> ', '</span>' ); ?>
            </div>
        <?php } ?>
        </header><!--.header-->
    <?php }
        $bpxl_standard_single = rwmb_meta( 'bpxl_standard_single_hide', $args = array('type' => 'checkbox'), $post->ID );
        if($bpxl_story_options['bpxl_single_featured'] == '1') {
            if(empty($bpxl_standard_single)) {
                if($bpxl_cover == '0' || $bpxl_cover == '') {
                    if ( has_post_thumbnail() ) { ?>
                        <div class="featured-single clearfix"><?php the_post_thumbnail('featured'); ?></div>
                    <?php }
                }
            }
        }
    ?>
    <div class="post-content single-post-content">
        <?php if($bpxl_story_options['bpxl_below_title_ad'] != '') { ?>
            <div class="single-post-ad">
                <?php echo $bpxl_story_options['bpxl_below_title_ad']; ?>
            </div>
        <?php }

        the_content();

        if($bpxl_story_options['bpxl_below_content_ad'] != '') { ?>
            <div class="single-post-ad">
                <?php echo $bpxl_story_options['bpxl_below_content_ad']; ?>
            </div>
        <?php }

            if($bpxl_story_options['bpxl_single_post_meta_options'][4] == '1') {
                the_tags('<div class="post-tags">'. __("Tags:","bloompixel") .' ', ', ', '</div>');
            }

            bpxl_wp_link_pages()
        ?>
    </div><!--.single-post-content-->
</article>