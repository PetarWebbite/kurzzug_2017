<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage storyblog
 * @since StoryBlog 1.0
 */

global $bpxl_story_options;

get_header(); ?>

<div class="main-wrapper clearfix">
    <div class="main-content <?php bpxl_layout_class(); ?>">
        <div class="content-area home-content-area">
            <div id="content" class="content content-home">
			
                <?php
                    if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
                    elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
                    else { $paged = 1; }

                    if($bpxl_story_options['bpxl_home_latest_posts'] == '1') {
                        $recent_cats = $bpxl_story_options['bpxl_home_latest_cat'];
                        $recent_cat = implode(",", $recent_cats);
                        $args = array(
                            'cat' => $recent_cat,
                            'paged' => $paged
                        );
                    } else {					
                        $args = array(
                            'paged' => $paged
                        );
                    }

                    // The Query
                    query_posts( $args );

                    if (have_posts()) : while (have_posts()) : the_post();

                    /*
                     * Include the post format-specific template for the content. If you want to
                     * use this in a child theme, then include a file called called content-___.php
                     * (where ___ is the post format) and that will be used instead.
                     */
                    
                    get_template_part( 'template-parts/post-formats/content', get_post_format() );

                    endwhile;

                    else:
                        // If no content, include the "No posts found" template.
                        get_template_part( 'template-parts/post-formats/content', 'none' );

                    endif;
                ?>
            </div><!--content-->
            <?php 
                // Previous/next page navigation.
                bpxl_paging_nav();
            ?>
        </div><!--content-area-->
    </div><!--.main-content-->
<?php get_footer(); ?>